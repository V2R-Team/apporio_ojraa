<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
error_reporting(0);

$query="select * from admin where admin_type = 0";
	$result = $db->query($query);
	$list=$result->rows;    
	       
?>


  <!-- Page Content Start --> 
  <!-- ================== -->

  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">View Sub Admin</h3>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email Id</th>
                        <th>Role</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                      foreach($list as $viewmodel){?>
                    <tr>
                      <td><?php echo $viewmodel['admin_id'];?></td>
                      
                      <td> 
                        <a title="Edit" href="home.php?pages=add-subadmin&edit_id=<?=$viewmodel['admin_id']?>" style="text-transform: capitalize;"><?=$viewmodel['admin_username']?></a>
                      </td>
                      
                      <td> 
			                 <?php
                           $carmodel=$viewmodel['admin_fname'];
                           if($carmodel=="")
                           {
                           echo "---------";
                           }
                           else
                           {
                           echo $carmodel;
                           }
                         ?>
                      </td>

                      <td> 
                       <?php
                           $carmodel=$viewmodel['admin_lname'];
                           if($carmodel=="")
                           {
                           echo "---------";
                           }
                           else
                           {
                           echo $carmodel;
                           }
                         ?>
                      </td>

                      <td> 
                       <?php
                           $carmodel32=$viewmodel['admin_email'];
                           if($carmodel32=="")
                           {
                           echo "---------";
                           }
                           else
                           {
                           echo $carmodel32;
                           }
                         ?>
                      </td>

                      <td> 
                        <?php 

                          $car_type_id=$viewmodel['admin_role'];

                          $query2="select * from role where role_id= '".$car_type_id."' ";
                            $result2 = $db->query($query2);
                            $list2=$result2->row;    


                          $car_type_name=$list2['role_name'];
                          if($car_type_name=="")
                             {
                             echo "---------";
                             }
                              else
                              {
                              echo $car_type_name;
                              }
                          ?>
                      </td>
                      
                    </tr>
                    <?php }?>
                    </tbody>
                    
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row --> 
    
  </div>
  
</section>
<!-- Main Content Ends -->

</body>
</html>
