<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="select * from rentcard INNER JOIN city ON rentcard.city_id=city.city_id INNER JOIN car_type ON rentcard.car_type_id=car_type.car_type_id INNER JOIN rental_category ON rentcard.rental_category_id=rental_category.rental_category_id ORDER BY rentcard.rentcard_id";
$result = $db->query($query);
$list=$result->rows;

if(isset($_GET['city'])){
    $query="select * from rentcard INNER JOIN city ON rentcard.city_id=city.city_id INNER JOIN car_type ON rentcard.car_type_id=car_type.car_type_id INNER JOIN rental_category ON rentcard.rental_category_id=rental_category.rental_category_id WHERE city.city_name='".$_GET['city']."' ORDER BY rentcard.rentcard_id ";
    $result = $db->query($query);
    $list=$result->rows;

}

$que="select * from city";
$res = $db->query($que);
$city_data = $res->rows;


if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE rentcard SET rentcard_admin_status='".$_GET['status']."' WHERE rentcard_id='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=rental-car");
}


if(isset($_POST['savechanges']))
{
    $query2="UPDATE rentcard SET price='".$_POST['price']."', price_per_hrs='".$_POST['price_hrs']."',price_per_kms='".$_POST['price_kms']."' where rentcard_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=rental-car");
}

//delete

if(isset($_POST['delete']))
{
    $delqry1="DELETE from rentcard where rentcard_id='".$_POST['delete']."'";
    $db->query($delqry1);
    $db->redirect("home.php?pages=rental-car");
}


?>


<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Rental Fare</h3>

            <span class="tp_rht">
            <a href="home.php?pages=add-rent" data-toggle="tooltip" title="" class="btn btn-primary add_btn" data-original-title="Add Rent"><i class="fa fa-plus"></i></a>
      </span>
        </div>


        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3" style="margin-bottom: 10px;">
                                <select class="form-control" name="Selectcity" onchange="window.location = this.options[this.selectedIndex].value">
                                    <option>Select Your City</option>
                                    <?php foreach($city_data as $city){ ?>
                                        <option value="home.php?pages=rental-car&city=<?php echo $city['city_name']; ?>"><?php echo $city['city_name']; ?></option>
                                    <?php } ?>

                                </select>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>City Name</th>
                                        <th>Car Name</th>
                                        <th>Package Name</th>
                                        <th>Package Price</th>
                                        <th width="8%">Status</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach($list as $rent){?>
                                        <tr>

                                            <td>
                                                <?php
                                                $city_name = $rent['city_name'];
                                                echo $city_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $car_type_name = $rent['car_type_name'];
                                                echo $car_type_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $rental_category = $rent['rental_category'];
                                                echo $rental_category;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $currency = $rent['currency'];
                                                $price = $rent['price'];
                                                echo $currency." ".$price;
                                                ?>
                                            </td>
                                            <?php
                                            if($rent['rentcard_admin_status']==1) {
                                                ?>
                                                <td class="">
                                                    <label class="label label-success" > Active</label>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="">
                                                    <label class="label label-default" > Deactive</label>
                                                </td>
                                            <?php } ?>
                                            <td>
                                                <div class="row action_row" style="width:100px;">

                                                    <span data-target="#<?php echo $rent['rentcard_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>

                                                    <span data-target="#delete<?php echo $rent['rentcard_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>
                                                    <?php if ($rent['rentcard_admin_status'] == 1) { ?>
                                                        <a data-original-title="Inactive" data-toggle="tooltip"
                                                           data-placement="top" class="btn menu-icon btn_eye_dis"
                                                           href="home.php?pages=rental-car&status=2&id=<?php echo $rent['rentcard_id']; ?>">
                                                            <i class="fa fa-eye-slash"></i> </a>
                                                    <?php } else { ?>
                                                        <a data-original-title="Active" class="btn menu-icon btn_eye"
                                                           href="home.php?pages=rental-car&status=1&id=<?php echo $rent['rentcard_id']; ?>">
                                                            <i class="fa fa-eye"></i> </a>
                                                    <?php } ?>

                                                </div>
                                            </td>
                                        </tr>
                                        <?php

                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>

<?php foreach($list as $rent){?>
    <div class="modal fade" id="<?php echo $rent['rentcard_id'];?>" role="dialog">
        <div class="modal-dialog">
            <form method="post" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fdetailsheading">Edit Price</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Price</label>
                                    <input type="text" class="form-control"  placeholder="Price" name="price" value="<?php echo $rent['price'];?>" id="price" required>
                                </div>
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Price Per Hour</label>
                                    <input type="text" class="form-control"  placeholder="Price/Hrs" name="price_hrs" value="<?php echo $rent['price_per_hrs'];?>" id="pricehrs" required>
                                </div>
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Price Per Kilometer</label>
                                    <input type="text" class="form-control"  placeholder="Price?Kms" name="price_kms" value="<?php echo $rent['price_per_kms'];?>" id="pricekms" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $rent['rentcard_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php }?>


<!--delete modal-->
<?php
foreach($list as $Delrentalcar){ ?>
    <div class="modal fade" id="delete<?php echo $Delrentalcar['rentcard_id'];?>" role="dialog">
        <div class="modal-dialog">
            <form method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fdetailsheading">Delete</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <h4>Do You Really Want To Delete This Package?</h4></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete" value="<?php echo $Delrentalcar['rentcard_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    </div>
<?php } ?>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>