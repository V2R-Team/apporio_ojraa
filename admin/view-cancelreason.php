<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="select * from cancel_reasons";
	$result = $db->query($query);
	$list=$result->rows;       
        
   if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE cancel_reasons SET cancel_reasons_status='".$_GET['status']."' WHERE reason_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-cancel");
    }
     
      if(isset($_POST['savechanges']))
	 
     {
	$query2="UPDATE cancel_reasons SET reason_name='".$_POST['reason_name']."' where reason_id='".$_POST['savechanges']."'";
	         $db->query($query2); 


 $db->redirect("home.php?pages=view-cancel");
	
	}

//delete

if(isset($_POST['delete']))
{
    $delqry1="DELETE from cancel_reasons where reason_id='".$_POST['delete']."'";
    $db->query($delqry1);
    $db->redirect("home.php?pages=view-cancel");
}
	
    
?>

<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Cancel Reason</h3>
      
      
      <span class="tp_rht">
         <a href="home.php?pages=add-cancel" data-toggle="tooltip" title="" class="btn btn-primary add_btn" data-original-title="Add Cancel Reason"><i class="fa fa-plus"></i></a>
      </span>
      
      
  </div>
  
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                    <th width="20%">Cancel Reason</th>
                    <th width="20%">Reason Type</th>
                      <th width="12%">Status</th>
                     <th width="10%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $cancelreason){?>
                  <tr>
                      <td>
                    <?php
            	      $reason_name = $cancelreason['reason_name'];
            	       echo $reason_name;
                    ?>
            	      </td>

                      <td>
                    <?php
            	      $reason_type = $cancelreason['reason_type'];
            	      if($reason_type==1){
            	       echo "Customer";
            	      }elseif($reason_type==2){
                          echo "Driver";
                      }
            	      else{
                          echo "Admin";
                      }
                    ?>
            	      </td>
            	     
                      <?php
                                if($cancelreason['cancel_reasons_status']==1) {
                                ?>
                                <td class="">
				    <label class="label label-success" > Active</label>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="">
				    <label class="label label-default" > Deactive</label>
                                </td>
                                <?php } ?>
         <td>
             <div class="row action_row" style="width:118px;">
             <span data-target="#<?php echo $cancelreason['reason_id'];?>" data-toggle="modal"><a data-original-title="Edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                            <span>
                                          
                                            <?php if($cancelreason['cancel_reasons_status']==2){ ?>
                                            
                                            <a data-original-title="Active" class="btn menu-icon btn_eye" href="home.php?pages=view-cancel&status=1&id=<?php echo $cancelreason['reason_id'];?>"> <i class="fa fa-eye"></i> </a>
                                            <?php } else { ?>
                                            <a data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis" href="home.php?pages=view-cancel&status=2&id=<?php echo $cancelreason['reason_id'];?>"> <i class="fa fa-eye-slash"></i> </a>
                                            <?php } ?>
                                                <span data-target="#delete<?php echo $cancelreason['reason_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>
                                             
                                           
                                           </span>


             </div>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div> 
</form>
<?php foreach($list as $cancelreason){?>
<div class="modal fade" id="<?php echo $cancelreason['reason_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Reason Name</h4>
      </div>
      <form  method="post"  onSubmit="return validatelogin()">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Reason Name</label>
                <input type="text" class="form-control"  placeholder="Reason Name" name="reason_name" value="<?php echo $cancelreason['reason_name'];?>" id="reason_name" required>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button type="submit" name="savechanges" value="<?php echo $cancelreason['reason_id'];?>" class="btn btn-info">Save Changes</button>
        </div>
      </form>
    </div>
    
    <!-- Modal content closed--> 
    
  </div>
</div>
<?php }?>


<?php
foreach($list as $DelReason){ ?>
    <div class="modal fade" id="delete<?php echo $DelReason['reason_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete</h4>
                </div>
                <form  method="post">
                    <div class="modal-body">
                        <div class="row">
                            <h4>Do You Really Want To Delete This Cancel Reason?</h4></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete" value="<?php echo $DelReason['reason_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>
<?php } ?>




<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>