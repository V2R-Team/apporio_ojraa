Rental - Car Type Api: November 13, 2017, 6:49 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => SEDAN
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 56
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
            [currency_iso_code] => QAR
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 56
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
            [currency_iso_code] => QAR
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => SUV
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 56
            [base_fare] => 178 Per 4 Miles
            [ride_mode] => 1
            [currency_iso_code] => QAR
            [currency_unicode] => 0
        )

)

city_name: Gurugram
latitude: 28.4120995288417
longitude: 77.0433047177062
-------------------------
