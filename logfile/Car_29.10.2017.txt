Rental - Car Type Api: October 29, 2017, 5:32 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => SEDAN
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 126
            [base_fare] => 80 Per 3 Km
            [ride_mode] => 1
            [currency_iso_code] => QAR
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 126
            [base_fare] => 90 Per 3 Km
            [ride_mode] => 1
            [currency_iso_code] => QAR
            [currency_unicode] => 0
        )

)

city_name: Doha
latitude: 25.350590196998716
longitude: 51.50239985436201
-------------------------
