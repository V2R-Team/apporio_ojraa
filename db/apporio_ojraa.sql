-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:21 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_ojraa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Ojraa', '', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '', 'ojraa@gmail.com', '', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'ojraa', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`) VALUES
(1, 'Ojraa', 'uploads/logo/logo_5a149908a3530.png', 'gabriel.bioscenter@gmail.com', 'Doha, Qatar', 'AIzaSyA-j38LyPy41VswOPZNNpj8kj77ER3z5EY', '25.2854473', '51.5310398', 'digitexcustomer');

-- --------------------------------------------------------

--
-- Table structure for table `All_Currencies`
--

CREATE TABLE `All_Currencies` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `All_Currencies`
--

INSERT INTO `All_Currencies` (`id`, `currency_name`) VALUES
(1, 'ARIARY'),
(2, 'AUSTRAL'),
(3, 'BHAT'),
(4, 'BIRR'),
(5, 'BOLIVAR'),
(6, 'BOLIVIANO'),
(7, 'CEDI'),
(8, 'CENT'),
(9, 'COLON'),
(10, 'CORDOBA'),
(11, 'CRUZEIRO'),
(12, 'DALASI'),
(13, 'DINAR'),
(14, 'DIRHAM'),
(15, 'DOBRA'),
(16, 'DOLLAR'),
(17, 'DONG'),
(18, 'DRACHMA'),
(19, 'DRAM'),
(20, 'ESCUDO'),
(21, 'EURO'),
(22, 'FLORIN'),
(23, 'FORINT'),
(24, 'FRANC'),
(25, 'GOURDE'),
(26, 'GUARANI'),
(27, 'GUILDER'),
(28, 'HRYVNIA'),
(29, 'KINA'),
(30, 'KIP'),
(31, 'KORUNA'),
(32, 'KRONE'),
(33, 'KUNA'),
(34, 'KWACHA'),
(35, 'KWANZA'),
(36, 'KYAT'),
(37, 'LARI'),
(38, 'LEK'),
(39, 'LEMPIRA'),
(40, 'LEONE'),
(41, 'LEU'),
(42, 'LEV'),
(43, 'LILANGENI'),
(44, 'LIRA'),
(45, 'LIVRE TOURNOIS'),
(46, 'LOTI'),
(47, 'MANTA'),
(48, 'METICAL'),
(49, 'MILL'),
(50, 'NAIRA'),
(51, 'NAKFA'),
(52, 'NGULTRM'),
(53, 'OUGUIYA'),
(54, 'PAANGA'),
(55, 'PATACA'),
(56, 'PENNY'),
(57, 'PESETA'),
(58, 'PESO'),
(59, 'POUND'),
(60, 'PULA'),
(61, 'QUETZAL'),
(62, 'RAND'),
(63, 'REAL'),
(64, 'RIAL'),
(65, 'RIEL'),
(66, 'RINGGIT'),
(67, 'RIYAL'),
(68, 'RUBEL'),
(69, 'RUFIYAA'),
(70, 'RUPEE'),
(71, 'RUPIAH'),
(72, 'SHEQEL'),
(73, 'SHILING'),
(74, 'SOL'),
(75, 'SOM'),
(76, 'SOMONI'),
(77, 'SPESMILO'),
(78, 'STERLING'),
(79, 'TAKA'),
(80, 'TALA'),
(81, 'TENGE'),
(82, 'TUGRIK'),
(83, 'VATU'),
(84, 'WON'),
(85, 'YEN'),
(86, 'YUAN'),
(87, 'ZLOTY'),
(88, 'QATAR');

-- --------------------------------------------------------

--
-- Table structure for table `application_currency`
--

CREATE TABLE `application_currency` (
  `application_currency_id` int(11) NOT NULL,
  `currency_name` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_currency`
--

INSERT INTO `application_currency` (`application_currency_id`, `currency_name`, `currency_iso_code`, `currency_unicode`) VALUES
(1, 'DOLLAR SIGN', 'AUD', '0024');

-- --------------------------------------------------------

--
-- Table structure for table `application_version`
--

CREATE TABLE `application_version` (
  `application_version_id` int(11) NOT NULL,
  `ios_current_version` varchar(255) NOT NULL,
  `ios_mandantory_update` int(11) NOT NULL,
  `android_current_version` varchar(255) NOT NULL,
  `android_mandantory_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_version`
--

INSERT INTO `application_version` (`application_version_id`, `ios_current_version`, `ios_mandantory_update`, `android_current_version`, `android_mandantory_update`) VALUES
(1, '233', 1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refused to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(13, 'i got lift', '', 0, 1, 1),
(14, 'tesr', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`) VALUES
(1, 'cus_BlbnFMdBaBCYn0', 12),
(2, 'cus_BlbnNqCWoervOA', 12),
(3, 'cus_BlbnNAPLH032du', 12),
(4, 'cus_BlbnwwHW8bMb57', 12),
(5, 'cus_BmdurKdRm1yuT7', 5);

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 1),
(2, 'Suzuki', 'uploads/car/car_2.png', 2),
(3, 'Ferrari', 'uploads/car/car_3.png', 1),
(4, 'Lamborghini', 'uploads/car/car_4.png', 1),
(5, 'Mercedes', 'uploads/car/car_5.png', 1),
(6, 'Tesla', 'uploads/car/car_6.png', 2),
(10, 'Renault', 'uploads/car/car_10.jpg', 1),
(11, 'taxi', 'uploads/car/car_11.png', 2),
(12, 'taxi', 'uploads/car/car_12.jpg', 2),
(13, 'yamaha', 'uploads/car/car_13.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `ride_mode`, `car_admin_status`) VALUES
(2, 'SEDAN', '', 'HATCHBACK', 'uploads/car/editcar_2.jpg', '100698', 1, 1),
(3, 'LUXURY', '', 'LUXURY', 'uploads/car/editcar_3.jpg', '31256', 1, 1),
(4, 'SUV', '', 'Mini', 'uploads/car/editcar_4.jpg', '23775', 1, 1),
(5, 'Karwa Taxi', '', 'TAXI', 'uploads/car/editcar_5.png', '', 1, 1),
(6, 'Sport', '', 'SEDAN', 'uploads/car/car_6.jpg', '', 1, 1),
(7, 'ECO CAR', '', 'VW', 'uploads/car/car_7.png', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `currency_iso_code`, `currency_unicode`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', 'QAR', 'QAR', '0', 'Km', '', '', 1),
(127, 'Wakrah', '25.1659314', '51.5975524', '', '', '', 'Km', '', '', 1),
(126, 'Doha', '25.2854473', '51.5310398', 'QAR', 'QAR', '0', 'Km', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_email`, `company_phone`, `company_address`, `country_id`, `city_id`, `company_contact_person`, `company_password`, `vat_number`, `company_image`, `company_status`) VALUES
(2, 'Digitax', 'maria16s.com@gmail.com', '3023184519', 'Santa Isabel, BogotÃ¡ - Bogota, Colombia', 'COLOMBIA', 124, 'Maria Fernanda Sarmiento ', 'bios2017**', '2419', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 15, 'k10', 1, 2, 1, 2, 'k10', 'Apporio Infolabs', 'k10', 'apporio@info.com2', '', 'k10', '', 'k10', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(18, 'lkdsasalsa', 'msammsa@qkjsa', 'kddepoew320-', 'saksalask'),
(17, 'qajkalk', 'SAJD@JSAG', '12344555666', 'kaal;al'),
(16, 'Yogesh', 'yogeshkumar2491@gmail.com', '', 'Testing'),
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!'),
(19, 'anurag', 'yuio@wkjlkew', '328740932', 'lfejljfsl'),
(20, 'sdllksd', 'skjs@lkdslk', 'lklkds', 'dlkslk'),
(21, 'dffhh', 'fhhjk@ghgj', '986987587', 'gkkhll'),
(22, 'asqwq', 'qaz@gmail.com', '2321235435', 'ertewtterterttrtrrttr');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '++91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_id` int(100) DEFAULT NULL,
  `currency_html_code` varchar(100) DEFAULT NULL,
  `currency_unicode` varchar(100) DEFAULT NULL,
  `currency_isocode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_id`, `currency_html_code`, `currency_unicode`, `currency_isocode`) VALUES
(6, 16, '&#x24;', '00024', 'USD '),
(7, 58, 'COP', '0', 'COP'),
(8, 88, 'Riyal', '65020', 'QAR');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 2, 'maria', 'maria', '3023184519', 'el cliente no me pagó absolutamente nada\n', 'Tuesday, Oct 10, 09:13 PM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 3, '28.4120482', '77.0433247', '28.4122167', '77.0432123', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:19:13 PM', '12:19:27 PM', '12:19:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(2, 4, '', '', '', '', '', '', '01:05:37 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 1, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(3, 5, '', '', '', '', '', '', '01:07:19 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 1, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(4, 6, '', '', '', '', '', '', '08:09:05 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 2, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(5, 7, '4.6035829', '-74.2159951', '4.6035829', '-74.2159951', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '05:22:09 AM', '05:23:04 AM', '05:23:56 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '9', '0.90', '8.10', '9.00', '0.00', '0.00 Km', '590.0', '1', '9', 1, '', 1, 0, '0000-00-00'),
(6, 12, '28.4121634997692', '77.0433346275958', '28.4121526871141', '77.0433269162449', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:57:13 PM', '12:57:18 PM', '12:57:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(7, 13, '28.4121813951325', '77.0433514752211', '28.4121813951325', '77.0433514752211', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:13:27 PM', '02:14:01 PM', '02:14:20 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(8, 15, '28.4121697', '77.0431898', '28.4121921', '77.0431898', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '03:14:51 PM', '03:14:55 PM', '03:15:03 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(9, 16, '28.4120279', '77.0433472', '28.4121697', '77.0431898', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '03:15:57 PM', '03:16:08 PM', '03:21:57 PM', '0', '0.00', '75.00', '0.00', '0.00', '0.00', 5, '175', '7.00', '168.00', '100.00', '0.00', '0.00 Miles', '0.0', '6', '175', 1, '', 1, 1, '0000-00-00'),
(10, 17, '28.4121697', '77.0431898', '28.4121697', '77.0431898', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '03:36:41 PM', '03:36:55 PM', '03:36:58 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 1, '0000-00-00'),
(11, 18, '28.4333553', '77.0361167', '28.421051', '77.0386959', '5-12, Mahashay Hansram Marg, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '70, Sohna Rd, Sector 48, Gurugram, Haryana 122004, India', '08:09:59 PM', '08:10:02 PM', '08:10:11 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(12, 20, '28.412153651033', '77.0432368946048', '28.4121889807548', '77.0433757827403', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:09:18 PM', '02:09:39 PM', '02:10:23 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(13, 25, '28.4121458616402', '77.0432842146294', '28.4120942965452', '77.043267170159', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:21:36 AM', '07:21:43 AM', '07:21:56 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(14, 26, '28.4121262451581', '77.04331217758', '28.4121262451581', '77.04331217758', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:31:19 AM', '07:31:23 AM', '07:31:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(15, 27, '28.4120829221915', '77.0432686381064', '28.4121844126176', '77.0433126670095', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:45:10 AM', '07:45:14 AM', '07:45:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(16, 28, '28.4121432', '77.0432503', '28.412139', '77.0432704', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '08:11:15 AM', '08:11:22 AM', '08:11:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 6, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(17, 29, '25.3258984', '51.5263326', '25.3258984', '51.5263326', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', '03:46:16 PM', '03:47:01 PM', '03:47:38 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 7, '80', '8.00', '72.00', '80.00', '0.00', '0.00 Km', '0.0', '1', '80', 1, '', 0, 1, '0000-00-00'),
(18, 30, '', '', '', '', '', '', '05:10:25 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 7, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(19, 31, '25.3258984', '51.5263326', '25.3258984', '51.5263326', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', '06:10:13 PM', '06:10:16 PM', '06:11:47 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 7, '80', '8.00', '72.00', '80.00', '0.00', '0.00 Km', '0.0', '2', '80', 1, '', 0, 1, '0000-00-00'),
(20, 33, '25.3258855', '51.5263223', '25.3258855', '51.5263223', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', '06:13:24 PM', '06:13:42 PM', '06:14:26 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 7, '80', '8.00', '72.00', '80.00', '0.00', '0.00 Km', '0.0', '1', '80', 1, '', 0, 1, '0000-00-00'),
(21, 37, '28.4120975908275', '77.0432371046217', '28.4121439728311', '77.0432476083542', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:19:48 PM', '01:19:53 PM', '01:19:58 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 38, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(22, 38, '28.4123387', '77.0434557', '28.4123417', '77.0434578', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '01:27:59 PM', '01:29:28 PM', '01:30:48 PM', '1', '10.00', '00.00', '0.00', '0.00', '0.00', 41, '110', '4.40', '105.60', '100.00', '0.00', '0.00 Miles', '0.0', '1', '110', 1, '', 1, 0, '0000-00-00'),
(23, 45, '28.4123406', '77.043456', '28.4123406', '77.043456', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '01:37:43 PM', '01:37:45 PM', '01:37:47 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 41, '100', '1.72', '41.28', '100.00', '43', '0.00 Miles', '0.0', '0', '57', 1, 'Insufficent Balance', 1, 1, '0000-00-00'),
(24, 47, '28.4124599676844', '77.0433882717761', '28.4124599676844', '77.0433882717761', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '09:35:09 AM', '09:35:19 AM', '09:35:35 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 42, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(25, 50, '28.4120657269221', '77.0432926560175', '28.4120656320218', '77.04328707546', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:05:43 AM', '10:05:47 AM', '10:05:57 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 43, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(26, 50, '28.4120657269221', '77.0432926560175', '28.4120656320218', '77.04328707546', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:05:43 AM', '10:05:47 AM', '10:05:57 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 43, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 0, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(27, 56, '28.4121106518697', '77.0432437677654', '28.4120589469285', '77.0432668423146', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:23:20 AM', '10:23:23 AM', '10:23:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 43, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 0, 'You Have Select Paypal', 0, 1, '0000-00-00'),
(28, 57, '28.4346477', '77.0350375', '28.4346477', '77.0350375', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '06:36:12 PM', '06:36:15 PM', '06:36:20 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(29, 58, '28.412319', '77.0434465', '28.412316', '77.0434411', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '05:05:45 AM', '05:05:49 AM', '05:05:58 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(30, 59, '28.4123366', '77.0434524', '28.4123375', '77.0434388', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '06:08:51 AM', '06:09:59 AM', '06:10:28 AM', '1', '10.00', '00.00', '0.00', '0.00', '0.00', 44, '110', '4.40', '105.60', '100.00', '0.00', '0.00 Miles', '0.0', '0', '110', 1, '', 1, 0, '0000-00-00'),
(31, 60, '28.4123196', '77.0434596', '28.412318', '77.0434589', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:15:00 AM', '06:15:02 AM', '06:15:05 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(32, 61, '28.4123376', '77.043449', '28.4123376', '77.043449', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '07:08:19 AM', '07:08:21 AM', '07:08:24 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(33, 62, '28.4123388', '77.0434472', '28.4123391', '77.0434491', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '07:08:47 AM', '07:08:49 AM', '07:08:53 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(34, 63, '28.4123151', '77.0434447', '28.4123151', '77.0434447', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '07:23:50 AM', '07:23:53 AM', '07:23:56 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(35, 64, '28.4123228', '77.0434474', '28.4123205', '77.0434452', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:11:39 AM', '09:11:40 AM', '09:11:48 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(36, 65, '28.4123311', '77.0434459', '28.412332', '77.0434464', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:12:24 AM', '09:12:27 AM', '09:12:33 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 44, '100', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(37, 66, '28.4121364681315', '77.0432776306542', '28.4121069821458', '77.0432476128951', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:42:05 AM', '10:42:09 AM', '10:42:17 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 49, '178', '0.00', '0.00', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(38, 67, '28.412093547357', '77.0432771887517', '28.412171671266', '77.0432706647356', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:45:26 AM', '06:45:30 AM', '06:45:49 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 55, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(39, 68, '28.4120803837328', '77.043191404235', '28.4120505687979', '77.0432939790825', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:55:57 AM', '06:56:01 AM', '06:56:44 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 55, '178', '0.00', '0.00', '178.00', '0.00', '0.00 Miles', '0.0', '1', '178', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(40, 69, '28.4120638165498', '77.043269253967', '28.4120636891841', '77.0432705646501', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:21:29 AM', '07:21:32 AM', '07:21:37 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 55, '178', '0.00', '0.00', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, 'You Have Select Paypal', 1, 1, '0000-00-00'),
(41, 71, '28.4122160123926', '77.0433010999831', '28.4121666010734', '77.04328232452', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:08:04 AM', '09:08:07 AM', '09:08:11 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 55, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(42, 73, '28.4121655661125', '77.0432625115194', '28.4120931147038', '77.0432838894666', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:20:30 AM', '10:20:59 AM', '10:21:19 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 55, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(43, 74, '28.4120748601908', '77.0432414729271', '28.4121570743601', '77.0432606166015', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:32:18 AM', '10:32:38 AM', '10:32:48 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 55, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(44, 76, '25.3503225', '51.5024861', '25.3503273', '51.5024919', 'Al magta, Doha, Qatar', 'Al magta, Doha, Qatar', '08:57:24 PM', '08:58:37 PM', '09:02:10 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 66, '80', '0.00', '0.00', '80.00', '0.00', '0.00 Km', '0.0', '4', '80', 1, 'You Have Select Paypal', 1, 0, '0000-00-00'),
(45, 77, '25.350322', '51.5024987', '25.3503067', '51.5025046', 'Al magta, Doha, Qatar', 'Al magta, Doha, Qatar', '09:14:34 PM', '09:14:51 PM', '09:18:45 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 66, '80', '8.00', '72.00', '80.00', '0.00', '0.00 Km', '0.0', '4', '80', 1, '', 1, 0, '0000-00-00'),
(46, 78, '28.4120742606377', '77.0432446768393', '28.4120616777434', '77.0432377447721', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:46:31 AM', '06:46:34 AM', '06:46:38 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 55, '178', '0.00', '0.00', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, 'You Have Select Paypal', 1, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `model_year` varchar(255) NOT NULL DEFAULT '',
  `model_color` varchar(255) NOT NULL DEFAULT '',
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0',
  `driver_image_first` varchar(255) NOT NULL DEFAULT '',
  `driver_image_second` varchar(255) NOT NULL DEFAULT '',
  `car_image_first` varchar(255) NOT NULL DEFAULT '',
  `car_image_second` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `model_year`, `model_color`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`, `driver_image_first`, `driver_image_second`, `car_image_first`, `car_image_second`) VALUES
(1, 0, 4, 'vishal', 'vishal@gmail.com', '97797646644', '', '123456', 'k6JMWPmLhwAJUmbi', '96', '0', '', 'd7UE5mbO3ZE:APA91bFn1sZCLUQaurhybYwEBe8NhnSeh2klcnR3T9iHTiL8YIOS67k6vPjs2Zaj0-5Pl2vW2Auz8p2ZNxRoER9Ei_qwkiQ15Zos9tdXAqI2MOokuolQzTBuUhoAxsayD-YKBPU6GBPP', 2, '2', 2, 3, '', '', '123456', 56, 'Tuesday, Oct 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4147384', '77.0461414', '', '13:07', 'Tuesday, Oct 10, 2017', 1, 0, 2, 2, 0, 1, 4, 0, '', 1, '', '2017-10-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_1.png', 'uploads/driver/driver_image_second1.png', 'uploads/driver/car_image_first1.png', 'uploads/driver/car_image_second1.jpg'),
(2, 0, 10, 'maria', 'maria16s.com@gmail.com', '3168905328', 'uploads/driver/1507641789driver_2.jpg', 'melq2419', 'yqk9OmAK0OPYEHcI', '8.1', '0', '', 'dk8ng1zQRBE:APA91bGkS2-npnlY8KyHehsMJN8ak-2cSQbD3iGk3Xuk16-uWWtxmPAFg8I1VNzF3BLElObOe2IQDkD-qQcLc0gmRO7KQ-RhMoCKvEQkTJNs1x7MDO61UO-_J3z1UsYqGCNa9oo2A4t_', 2, '2.5', 3, 31, '', '', 'myf2419', 124, 'Tuesday, Oct 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '4.6037208', '-74.2160639', '----', '07:49', 'Wednesday, Oct 11, 2017', 1, 4, 1, 1, 0, 1, 2, 0, '', 1, '', '2017-10-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_2.jpg', 'uploads/driver/driver_image_second2.png', 'uploads/driver/car_image_first2.jpg', 'uploads/driver/car_image_second2.png'),
(3, 0, 4, 'shilpa', 'shilpa@apporio.com', '9865321478', '', '123456', '3UYwNBCjqymHksn4', '0', '0', '', 'c4kSr7-NejE:APA91bHGsQ81wzd2KnBJoZd3IxiVZbYuOvxm-TztPEUGtiAstIiDbmUgs2qmyQjm-r1tcdRabC8Kqmh8FChGKTKgnIYSi_n0mo_U3McsqB_Tl8jIMtOnxDSYlHffyibjUWeMYgUnLPNL', 2, '', 2, 3, '', '', '1234', 56, 'Tuesday, Oct 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122189', '77.0432347', '----', '04:03', 'Tuesday, Oct 10, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(4, 0, 5, 'qwerty', 'q@9.com', '1234512345', 'uploads/driver/1508236962driver_4.jpg', 'qwerty', 'XMNqVmJZj5Dmyev0', '570', '0', '', 'EE78917C44EEC35D83D1E19FD4AF4E4B78BC9FE7E32BA59B8FB5040BB020A95D', 1, '2.33333333333', 3, 20, '', '', '1234', 56, 'Thursday, Oct 12', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121283966258', '77.043272887309', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '08:54', 'Wednesday, Nov 8, 2017', 6, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-12', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_7.png', 'uploads/driver/driver_image_second7.png', 'uploads/driver/car_image_first7.png', 'uploads/driver/car_image_second7.png'),
(5, 0, 4, 'adam', 'adam@gmail.com', '7878787878', '', '123456', 'doNfOZYqhPTucApl', '456', '0', '', 'dltQh-ye2nY:APA91bH0yRLf3XK5bQ5W0nly7kAthg_p-XrDcRSPuUGZ9I7rMul-QUiEm1a9QpeM0f8ZPPOIlkwEFMcp2uhPcj9EX96zO_NEmGHuraB0geTHdmRrSexXKNiuUbBC5iS8mde3q7kVnjOa', 2, '1.33333333333', 2, 3, '', '', '1212', 56, 'Thursday, Oct 12', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4317705', '77.0356924', '----', '11:43', 'Tuesday, Oct 17, 2017', 4, 1, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-10-12', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(6, 0, 4, 'bzbzb', 'zhhzh@gmail.com', '996996969666', '', 'sbsbbsbsbssb', 'bDcO2nbvFOvjpjTH', '96', '0', '', 'fUniqxYJpgE:APA91bGbCUQT2AVHSFy0h8eXWTsl13KCXmUbtpx16U1rlTwv-kXoMEoJtpWc7xLTv55qpIQdMaDi8nG-4fu05pLV6nmPFFBVY16rBs8m6fUaKU53Wy1hcBuTfzW5lkKbf6FT6XqvWgO4', 2, '0', 2, 3, '', '', 'nzzbnzbzbz', 56, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121434', '77.0432658', '----', '08:11', 'Monday, Oct 23, 2017', 1, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_6.jpg', 'uploads/driver/driver_image_second6.jpg', 'uploads/driver/car_image_first6.jpg', 'uploads/driver/car_image_second6.jpg'),
(7, 0, 10, 'khaldoun the driver', 'k.baz@qmobileme.com', '0097455642929', '', 'qmobileme', 'DQWt5gscepNEEcQ1', '216', '0', '', 'e5OOYfpb5-M:APA91bHwwRiJvFo_nKVxq9khAFslkI5WC_Q9yJBtG3ORp5L6C5WgW6bJWI0bMcKnUqQas3ij6dFASyVTx3TcvUFD7rF8wQe1IsBKcqH9n3t4kNnS4xXnmsq7iR22jwyU3HVOqqylgX7h', 2, '2.5', 2, 0, '', '', '6666655', 126, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '33.6695535', '35.5998233', '----', '04:47', 'Tuesday, Nov 21, 2017', 3, 0, 1, 2, 0, 2, 4, 0, '', 1, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_7.jpg', 'uploads/driver/driver_image_second7.jpg', 'uploads/driver/car_image_first7.jpg', 'uploads/driver/car_image_second7.jpg'),
(8, 0, 5, 'sg', 'sgoyL@g.vom', '1332445', '', '123456', 'z9zroHzwtDqs8pjM', '0', '0', '', 'fWy5UetrnWw:APA91bEFWzFG3HEz0VjJNTK6tOrCBNpq1F-Io32T1J0qQogOVx4C9ofPTDuhu8I3SdXu7zBOmJMCRYHMFatKeuNg6T4RKpryeAgu5LAs_SRsYev7HJVCXd-8MWeZ4GuhYo2qRx6PByUR', 2, '', 3, 2, '', '', 'we wee', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122187', '77.0432289', 'Not yet fetched', '06:44', 'Tuesday, Oct 24, 2017', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_1.png', 'uploads/driver/driver_image_second1.png', 'uploads/driver/car_image_first1.jpg', 'uploads/driver/car_image_second1.jpg'),
(22, 0, 4, 'sbgsss', 'wwhsh@gmail.com', '65659595595', '', 'dbdhddhdhd', 'HTqpnB9Wl0eVhepE', '0', '0', '', '', 0, '', 2, 3, '', '', 'shghdhdd', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(9, 0, 6, 'hhhhh', '7@7.com', '2222222222', '', 'qwerty', 'bZrwI6M5MVemtN3n', '0', '0', '', 'E7F7701CDDB1563986651F2ECBED05C1D4A0CFB0DD421463EB64D9D906B5B0BE', 1, '', 4, 11, '', '', '1234', 56, 'Wednesday, Nov 8', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '12:37', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-08', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(10, 0, 5, 'hiking', '6@9.com', '1234567822', '', 'qwerty', 'iZ2GqT45atinDf0R', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 3, 20, '', '', '1234', 56, 'Wednesday, Nov 8', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '08:59', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-08', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(11, 0, 4, 'bjbhhn', '5@5.com', '1111111111', '', 'qwerty', 'FqdVwARgUwSBqYto', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Wednesday, Nov 8', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '12:24', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-08', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(12, 0, 4, 'mojkl', '7@9.com', '5555555555', '', 'qwerty', 'vrPGDkScRJyaN8OP', '0', '0', '', '996B89F1F4E6906B5C6273396AF2557368E9D5C10BB93068F7EE1AB21F24DA7D', 1, '', 2, 3, '', '', '1234', 56, 'Wednesday, Nov 8', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '10:55', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-08', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(13, 0, 4, 'shahhs', '7@1.com', '6666666666', '', 'qwerty', 'rvwfQPnzcOFiuQ8w', '0', '0', '', '996B89F1F4E6906B5C6273396AF2557368E9D5C10BB93068F7EE1AB21F24DA7D', 1, '', 2, 3, '', '', '1234', 56, 'Wednesday, Nov 8', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '10:44', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-08', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(14, 0, 4, 'eryff', '1@9.com', '3333333333', '', 'qwerty', 'vU8fV3nXZGr6EXO8', '0', '0', '', '996B89F1F4E6906B5C6273396AF2557368E9D5C10BB93068F7EE1AB21F24DA7D', 1, '', 2, 3, '', '', '1234', 56, 'Wednesday, Nov 8', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '10:56', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-08', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(15, 0, 4, 'hello', '9@0.com', '1234563214', '', 'qwerty', 'KwRBqhowqC45UFFe', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '12:43', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(16, 0, 4, 'your', '67@09.com', '1234569870', '', 'qwerty', 'YC6KRbb5KPI7wuI2', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '01:10', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(17, 0, 4, 'try', '7@78.com', '0987654123', '', 'qwerty', 'QU2fVIAVny1EPqx1', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '01:07', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(18, 0, 4, 'tyre', '67@90.com', '1234562222', '', 'qwerty', 'pv79kzB0N2JtxRzL', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '01:57', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(19, 0, 4, 'djdjd', '5@0.com', '4567890123', '', 'qwerty', 'GMN59fLUXqip7pIc', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '02:10', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(20, 0, 4, 'qwer', '5@90.com', '7890456123', '', 'qwerty', 'UqdYu0STpSvE0pes', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '02:05', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(21, 0, 4, 'dhdhdh', '34@90.com', '1234564123', '', 'qwerty', '5a9gei3ENb17csUJ', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '07:48', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(23, 0, 4, 'ddff', 'op@90.com', '1234560879', '', 'qwerty', 'zUmrJq27x6BM2TqC', '0', '0', '', '4BB0E427A625AECF8A3C243C1864550DE15EED4F9FCD5A06D5947A24D23AD9F8', 1, '', 2, 3, '', '', '1234', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '07:50', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(24, 0, 10, 'fffn', 'sgsh@gmail.com', '89898989898', '', 'zbbzzbzbz', 'Tdm6oPAAVr1glrLa', '0', '0', '', '', 0, '', 2, 3, '', '', 'vxxvxvxbx', 126, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(25, 0, 4, 'xnxnn', 'sjsjdj@g.com', '9597949497', '', 'dbdbbdbbdbdbd', '8IphgFUiPJPOgPh7', '0', '0', '', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '', 2, 3, '', '', 'ccv', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123291', '77.0434325', '----', '10:53', 'Monday, Nov 13, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(26, 0, 4, 'dfddf', 'ff@9.com', '2222222221', '', 'qwerty', 'Lsm7xHwG0o4PQDzM', '0', '0', '', 'AF01E192488E9BD0FC63627AEF73BD716329B2E9302657B823273FD0A3CD33A6', 1, '', 2, 3, '', '', '1234', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121212751681', '77.0432758388657', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '11:35', 'Monday, Nov 13, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_26.jpg', 'uploads/driver/driver_image_second26.jpg', 'uploads/driver/car_image_first26.jpg', ''),
(27, 0, 4, 'hhrehe', 'bsssbb@gmail.com', '499779797', '', 'bszbbzbzbzz', 'wQtBl5S1w7gODgXR', '0', '0', '', '', 0, '', 2, 3, '', '', 'bzbzbz', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(28, 0, 4, 'xjxnjx', 'shhzh@gmail.com', '768987897', '', 'zbbzzbzbz', 'CkOLixFfwifNdWMP', '0', '0', '', '', 0, '', 2, 3, '', '', 'ddffr', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(29, 0, 4, 'xhdhxx', 'dhdhd@g.com', '9034787671', '', '123456', '9BCqHamBhbU7hXq1', '0', '0', '', '', 0, '', 2, 3, '', '', 'zhhxxhx', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 4, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_29.jpg', 'uploads/driver/driver_image_second29.jpg', 'uploads/driver/car_image_first29.jpg', 'uploads/driver/car_image_second29.jpg'),
(30, 0, 4, 'djxjdx', 'hshzjd@g.com', '8080808080', '', '123456', '8QCW99wROxWTQYZr', '0', '0', '', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '', 2, 3, '', '', 'zb,b,bz', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.41233', '77.0434563', '----', '11:42', 'Monday, Nov 13, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_30.jpg', 'uploads/driver/driver_image_second30.jpg', 'uploads/driver/car_image_first30.jpg', 'uploads/driver/car_image_second30.jpg'),
(31, 0, 4, 'ejsjs', 'shshss@g.com', '3636363636', '', '123456', '5lt91oxB0FQpe1ky', '0', '0', '', '', 0, '', 2, 3, '', '', 'bzbzzbx', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 4, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_31.jpg', 'uploads/driver/driver_image_second31.jpg', 'uploads/driver/car_image_first31.jpg', 'uploads/driver/car_image_second31.jpg'),
(32, 0, 4, 'dncn', 'dncj@5.com', '4444444444', '', 'qwerty', '0bTW1eW8Jb4f4NqR', '0', '0', '', 'AF01E192488E9BD0FC63627AEF73BD716329B2E9302657B823273FD0A3CD33A6', 1, '', 2, 3, '', '', '1223', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '12:54', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', 'uploads/driver/car_image_first32.jpg', ''),
(33, 0, 4, 'sbssbs', 'bebdb@g.com', '2525252525', '', '123456', 'bP91WOKOxTTJX3M5', '0', '0', '', '', 0, '', 2, 3, '', '', 'sbbssbsbs', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_33.jpg', 'uploads/driver/driver_image_second33.jpg', '', ''),
(34, 0, 4, 'vhh', 'bbsb@g.com', '9479898989', '', 'hdbbddbdb', 'KHebbLuEKWow6spz', '0', '0', '', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '', 2, 3, '', '', 'bzbzbz', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123381', '77.0434495', '----', '11:50', 'Monday, Nov 13, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_34.jpg', 'uploads/driver/driver_image_second34.jpg', 'uploads/driver/car_image_first34.jpg', 'uploads/driver/car_image_second34.jpg'),
(35, 0, 4, 'sdkdks', 'snjssn@g.com', '8989898994495', '', 'shhssbzb', 'JUuGETBx07LyEeDO', '0', '0', '', '', 0, '', 2, 3, '', '', 'bzbbzxx', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(36, 0, 4, 'sbzzbz', 'ssbsbs@g.com', '7878787879', '', '123456', 'JRJ9tVhbrU4esHLo', '0', '0', '', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '', 2, 3, '', '', '2vezvzb', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4346475', '77.0350375', '----', '05:34', 'Monday, Nov 13, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_36.jpg', 'uploads/driver/driver_image_second36.jpg', 'uploads/driver/car_image_first36.jpg', 'uploads/driver/car_image_second36.jpg'),
(37, 0, 4, 'bababsb', 'bdbs@1.com', '3333333332', '', 'qwerty', 'F4p2j1UoHbHGpA3S', '0', '0', '', 'AF01E192488E9BD0FC63627AEF73BD716329B2E9302657B823273FD0A3CD33A6', 1, '', 2, 3, '', '', '1234', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '01:01', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', 'uploads/driver/car_image_first37.jpg', ''),
(38, 0, 4, 'ckcjxj', 'glg@4.com', '5555555556', '', 'qwerty', 'ud3iRnkAGrBt2jC4', '96', '0', '', 'AF01E192488E9BD0FC63627AEF73BD716329B2E9302657B823273FD0A3CD33A6', 1, '0', 2, 3, '', '', '1234', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121227475344', '77.0432218047267', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '13:21', 'Monday, Nov 13, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_38.jpg', 'uploads/driver/driver_image_second38.jpg', 'uploads/driver/car_image_first38.jpg', 'uploads/driver/car_image_second38.jpg'),
(39, 0, 5, 'bdndb', 'fjfncj@0.com', '7777777772', '', 'qwerty', 'pXQ8BlmtGLM8YbLJ', '0', '0', '', 'AF01E192488E9BD0FC63627AEF73BD716329B2E9302657B823273FD0A3CD33A6', 1, '', 3, 2, '', '', '1234', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '09:53', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(40, 0, 4, 'vvvvvhh', 'uu@g.com', '5989889895', '', 'sbsdnxnxxn', 'Rqo5eHdb9TgDTxHv', '0', '0', '', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '', 2, 3, '', '', 'nxnxxnxn', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4346477', '77.0350375', '----', '06:26', 'Tuesday, Nov 14, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_40.jpg', 'uploads/driver/driver_image_second40.jpg', 'uploads/driver/car_image_first40.jpg', 'uploads/driver/car_image_second40.jpg'),
(41, 0, 4, 'anurag', 'anurag@apporio.com', '8874531856', 'uploads/driver/1510665990driver_41.jpg', 'qwerty', '08uOGgMBOfYzYiph', '201.6', '0', '41.28', 'dbhWoGDWT-4:APA91bGhdWAO40MngxNMFIchnyr-syWWhd9OIFJz4bISQVaTv-ZCiGxj0qClk2j08h9WnyehgIrknJDDbuUyWvOElsGZFoM4mOACdKscqJ1mZXWaDe4P-pBAhi8JmcR8I16t7L6rmPml', 2, '2.5', 2, 3, '', '', '1234', 56, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123258', '77.0434445', '----', '12:57', 'Thursday, Nov 16, 2017', 2, 1, 1, 2, 0, 1, 2, 0, '', 1, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_41.jpg', 'uploads/driver/driver_image_second41.jpg', 'uploads/driver/car_image_first41.jpg', 'uploads/driver/car_image_second41.jpg'),
(42, 0, 5, 'hdhd', 'guru@56.com', '2222222202', '', 'qwert', 'dmpVBQKWwzWynO8R', '95', '0', '', '711B40D932B03FA3E0C8F99F2CBB0479580848D82EFC706A4EDDA668EB9BB006', 1, '', 3, 20, '', '', '1234', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121336182844', '77.0432914607944', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '09:55', 'Wednesday, Nov 15, 2017', 1, 0, 1, 2, 0, 2, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_42.jpg', 'uploads/driver/driver_image_second42.jpg', 'uploads/driver/car_image_first42.jpg', 'uploads/driver/car_image_second42.jpg'),
(43, 0, 5, 'hxxh', 'djdj@00.com', '3333333331', '', 'qwerty', 'ZSOqr61Q6wKKe3QO', '95', '0', '', 'AF01E192488E9BD0FC63627AEF73BD716329B2E9302657B823273FD0A3CD33A6', 1, '0', 3, 20, '', '', '1234', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121106518697', '77.0432437677654', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '10:23', 'Wednesday, Nov 15, 2017', 2, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_43.jpg', 'uploads/driver/driver_image_second43.jpg', 'uploads/driver/car_image_first43.jpg', 'uploads/driver/car_image_second43.jpg'),
(44, 0, 4, 'sjjdhd', 'shdhhd@g.com', '5959898989', '', 'dbhdjfjdj', 'ajSiyv4WAJ1My5FV', '979.2', '0', '192', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '2.14705882353', 2, 3, '', '', 'hsshshshhs', 56, 'Thursday, Nov 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123477', '77.0434607', '----', '09:51', 'Saturday, Nov 18, 2017', 11, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_44.jpg', 'uploads/driver/driver_image_second44.jpg', 'uploads/driver/car_image_first44.jpg', 'uploads/driver/car_image_second44.jpg'),
(45, 0, 10, 'khaldoun baz', 'k.baz2@qmobileme.com', '97455642929', '', 'qmobileme', 'jmpf2USD4znMkIBH', '0', '0', '', '', 0, '', 2, 3, '', '', '5555f', 126, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_45.jpg', 'uploads/driver/driver_image_second45.jpg', 'uploads/driver/car_image_first45.jpg', ''),
(46, 0, 10, 'khaldoun baz', 'khillo_baz@hotmail.com', '974556429291', '', 'qmobile', 'Y1kAZEYR8VKzjEM8', '0', '0', '', 'd3PkaxfuHco:APA91bHb6U-voHO6YO6uyOtTsW_UYGeWnlxsMqm5ghQNZQyHTlehrD2L2nYst23cnefsm0qws-v2tcFK31MPZwrSWNa55zBF3Jltov_s_xJopuZhXsk7yU-JrwExVxnana1AcpiBs3Ht', 2, '', 3, 20, '', '', '522224111', 126, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '33.6702934', '35.6001475', '----', '07:27', 'Monday, Nov 20, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3, 'uploads/driver/driver_first_image_46.jpg', 'uploads/driver/driver_image_second46.jpg', 'uploads/driver/car_image_first46.jpg', 'uploads/driver/car_image_second46.jpg'),
(47, 0, 4, 'manudh', 'sjsjs@gmcok', '977998988989', 'uploads/driver/1510999400driver_47.jpg', 'bsshbssb', 'Fa73yJytwTUMy3wC', '0', '0', '', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '', 2, 3, '', '', 'jsjs', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.412356', '77.0434509', '----', '10:09', 'Saturday, Nov 18, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_47.jpg', 'uploads/driver/driver_image_second47.jpg', 'uploads/driver/car_image_first47.jpg', 'uploads/driver/car_image_second47.jpg'),
(48, 0, 4, 'isjsj', 'ejdjd@g.com', '464646644', '', 'ehhshhdh', '7urhVAa0JW9QLWcI', '0', '0', '', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '', 2, 3, '', '', 'hshshsh', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122931', '77.0434092', '----', '08:51', 'Monday, Nov 20, 2017', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_48.jpg', 'uploads/driver/driver_image_second48.jpg', 'uploads/driver/car_image_first48.jpg', 'uploads/driver/car_image_second48.jpg'),
(49, 0, 6, 'jfbccjfh', 'chch@90.com', '1414141414', '', 'qwerty', '516sYAMUCNadPGZ0', '167.32', '10.68', '', '711B40D932B03FA3E0C8F99F2CBB0479580848D82EFC706A4EDDA668EB9BB006', 1, '4.5', 4, 11, '', '', '1234', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121905208064', '77.0432112407005', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '04:45', 'Wednesday, Nov 22, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_49.jpg', 'uploads/driver/driver_image_second49.jpg', 'uploads/driver/car_image_first49.jpg', 'uploads/driver/car_image_second49.jpg'),
(50, 0, 6, 'baba', 'hava@7.com', '8888866666', '', 'qwerty', '1oJC6nLUtxkGp3YU', '0', '0', '', 'E7F7701CDDB1563986651F2ECBED05C1D4A0CFB0DD421463EB64D9D906B5B0BE', 1, '', 4, 11, '', '', '1234', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '12:37', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(51, 0, 10, 'Khaldoun Baz', 'k.bazee@qmobileme.com', '974556429292', '', 'aaabbbccc', 'w3URRvIpczNHPmHj', '0', '0', '', '', 0, '', 2, 3, '', '', 'ggggg', 126, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(52, 0, 4, 'hddh', 'hdddhxjd@g.com', '76989879799', '', 'shhshsjsj', 'VYCaPT7YUV6g7aM4', '0', '0', '', '', 0, '', 2, 3, '', '', '1234', 56, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(53, 0, 4, 'sjjsjJ.sshssh', 'hdddjdj@g.com', '767979998989', '', 'ddhdhd', 'PZjaAoftgWa1Sxlo', '0', '0', '', '', 0, '', 2, 3, '', '', 'xbxbxxbx', 56, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_53.jpg', '', '', ''),
(54, 0, 10, 'khaldoun', 'khillo_baz333@hotmail.com', '9745564292911', '', 'qmobile', 'gWj0FDPh6UHEpDEO', '0', '0', '', '', 0, '', 2, 3, '', '', '55565', 126, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(55, 0, 6, 'beet', 'zgh@90.com', '2222211111', 'uploads/driver/1511260531driver_55.jpg', 'qwerty', 'JgTSao6Sbft3ozyl', '1171.24', '10.68', '', '5967FBC70615B5DC158EB5AE96C70B255D10770FF59BBDD794C662E64568D7BB', 1, '1.42857142857', 4, 11, '', '', '1234', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120967896498', '77.0432984056016', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '06:48', 'Wednesday, Nov 22, 2017', 7, 1, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_55.jpg', 'uploads/driver/driver_image_second55.jpg', 'uploads/driver/car_image_first55.jpg', 'uploads/driver/car_image_second55.jpg'),
(56, 0, 10, 'nayoof', 'md@qmobile.me', ' 97455320001', '', '123456', '9U2ZUrBDWdzt9y9T', '0', '0', '', 'eLDQuxGcbMc:APA91bH0KQt07bcf9Tg8NuEeskl-e5gkB2odpg0uGR4_y_pox-ByORsvLEkf_Z0tQp0WVXTO_AYPe625HNxDvueSSOK4dn15X263FlE-Sn4BYzQ9p_6g0MQ-9lgbw8Xh_aUwkFV0HQHM', 2, '', 2, 0, '', '', '26002', 126, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.3503425', '51.5025336', '----', '22:26', 'Tuesday, Nov 28, 2017', 0, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_56.jpg', 'uploads/driver/driver_image_second56.jpg', 'uploads/driver/car_image_first56.jpg', 'uploads/driver/car_image_second56.jpg'),
(57, 0, 10, 'nayoof1', 'md1@qmobile.me', ' 97433343001', '', '123456', 'DxX43zG2vz6LlY3D', '0', '0', '', '', 0, '', 2, 0, '', '', '64422', 126, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_57.jpg', 'uploads/driver/driver_image_second57.jpg', 'uploads/driver/car_image_first57.jpg', 'uploads/driver/car_image_second57.jpg'),
(58, 2, 10, 'rashwan', 'test@test.test', '55320001111', '', 'qmobileme', '', '0', '0', '', '', 0, '', 2, 3, '', '', '10000', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-11-21', 1, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3, '', '', '', ''),
(59, 0, 4, 'vzvzhz', 'svzhzhj@g.com', '9034787672', '', '123456', '43VSQqSwVTddkZgi', '0', '0', '', 'dg63mBx4E2w:APA91bHmejIvQHSnjOrnBsQTheqmDH8j_A0Ybbhno-3Vl34eqfnPDQmDe4DrBRLQRmngqQzQK7oVB7ji47usj0UHG90o9dEKeyuOD_d-BTf2UJY8g56bPlcwpY__2VLquLKi2l3bDg2Z', 2, '', 2, 3, '', '', 'zbbxh', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4346505', '77.0350439', '----', '03:32', 'Tuesday, Nov 21, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_59.jpg', 'uploads/driver/driver_image_second59.jpg', 'uploads/driver/car_image_first59.jpg', 'uploads/driver/car_image_second59.jpg'),
(60, 0, 4, 's h dhd', 'jddjd@g.com', '68869899898', '', 'xnddjjdjdj', 'ehUSgMBmKVzYMFxS', '0', '0', '', '', 0, '', 2, 3, '', '', 's8dudu', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', 'uploads/driver/car_image_first60.jpg', 'uploads/driver/car_image_second60.jpg'),
(61, 0, 10, 'khaldoun baz', 'k.baz@osoolmedia.com', ' 974556429293', '', 'savoir', 'CYyHA1nEuoNrR5iO', '0', '0', '', '', 0, '', 3, 31, '', '', 'tfdddd', 126, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(62, 0, 4, 'bdhdeh', 'hshsshs@g.com', '59955959595', '', 'dbdbdbdb', 'jFPiOhQq9XQZUROB', '0', '0', '', '', 0, '', 2, 3, '', '', 'zbzbz', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(63, 0, 4, 'snsjsj', 'sjshjs@g.com', '979797977979', '', 'dxhxjxjjxj', 'BeZoBTGHUxN4044D', '0', '0', '', '', 0, '', 2, 3, '', '', 'bzbzbs', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(64, 0, 4, 'gh4jjsw', 'hdhddhd@g.com', '5995989898', '', 'jddjjxn', 'QA9d9qyk4QKHBE4w', '0', '0', '', '', 0, '', 2, 3, '', '', 'vbbzxbxb z', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_64.jpg', '', '', ''),
(65, 0, 10, 'khaldoun', 'bazkhaldoun@gmail.com', ' 974556459293', '', 'qmobilepass', 'JAcOA2XrjBVChOnj', '0', '0', '', '', 0, '', 2, 3, '', '', '5555554', 126, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_65.jpg', 'uploads/driver/driver_image_second65.jpg', 'uploads/driver/car_image_first65.jpg', 'uploads/driver/car_image_second65.jpg'),
(66, 0, 10, 'osool', 'md@osoolmedia.com', '0097430320001', '', '123456', 'SpxAwGoVsHU60fS0', '144', '16', '', 'fk-cIwkJhh8:APA91bFoktlU5WvMDpp79oeOalgtebR3X6NAfyBPyfD269fYKcE-VPBPlE6wQPbmReJ99tW2lNjHtS81ekTVQfi3F-s4BeLW4WTC7NbnoNwD4fFssCDqAskrAEgAbxY0WPoE9Woh9X6b', 2, '0', 2, 0, '', '', '65432', 126, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '25.3503038', '51.5024927', '----', '21:19', 'Tuesday, Nov 21, 2017', 2, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_66.jpg', 'uploads/driver/driver_image_second66.jpg', 'uploads/driver/car_image_first66.jpg', 'uploads/driver/car_image_second66.jpg'),
(67, 0, 20, 'khaldoun', 'k.baz@qmobilecom.com', '00974556429293', '', 'aabbcc', 'Q0JtPeiDngrGNtFu', '0', '0', '', '', 0, '', 4, 11, '', '', '555553333', 126, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(68, 0, 4, 'khaldoun', 'ggg@uuuu.hhh', '00974556429265', '', 'ddddddd', 'VU7veUs9amx2lSGW', '0', '0', '', '', 0, '', 2, 3, '', '', '555555', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(69, 0, 4, 'hvggt', 'fgyy@g.com', '5556668865', '', 'gggftg', 'K026gE8TumUO58Wz', '0', '0', '', '', 0, '', 2, 3, '', '', 'ggg', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_69.jpg', 'uploads/driver/driver_image_second69.jpg', 'uploads/driver/car_image_first69.jpg', 'uploads/driver/car_image_second69.jpg'),
(70, 0, 4, 'gfgg6', '6t7y@g.com', '2323232323', '', '123456', 'YMzXsk2IowJmalxB', '0', '0', '', '', 0, '', 2, 3, '', '', 'cfhhh', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_70.jpg', '', '', ''),
(71, 0, 4, 'vbh', 'vbbb@g.com', '424242425999', '', '123456', 'CFSla7RIv7JTx5Ef', '0', '0', '', '', 0, '', 2, 3, '', '', '12312', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(72, 0, 4, 'hhhhhg', 'tdhjkvchj@g.com', '899999999999', '', 'dfvvvv', 'LAagLDm69eQrykQJ', '0', '0', '', '', 0, '', 2, 3, '', '', 'ggghh', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', 'uploads/driver/car_image_first72.jpg', ''),
(73, 0, 4, 'banco', '12@90.com', '5555533333', '', 'qwerty', 'IfUzie2Sn8pql0Ow', '0', '0', '', '711B40D932B03FA3E0C8F99F2CBB0479580848D82EFC706A4EDDA668EB9BB006', 1, '', 2, 3, '', '', '1234', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '07:19', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(74, 0, 4, 'dnndcnccjrjr', 'jrrjjf@g.com', '959559595', '', '123456', 'zI7qXTWbeRjwCxG6', '0', '0', '', '', 0, '', 2, 3, '', '', 'hhddh', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_74.jpg', 'uploads/driver/driver_image_second74.jpg', 'uploads/driver/car_image_first74.jpg', ''),
(75, 0, 4, 'shzbzb', 'hhxhxj@g.com', '89888.88..', '', 'dbbxbx', 'c1hCSovIAvSXrDj8', '0', '0', '', '', 0, '', 2, 3, '', '', 'bzbzbz', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_75.jpg', 'uploads/driver/driver_image_second75.jpg', '', ''),
(76, 0, 5, 'demo', 'demo6699@g.com', '9797979766', '', '123456', 'MzdEeRopWj3vrmb5', '0', '0', '', '', 0, '', 3, 20, '', '', '8899', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_76.jpg', '', '', ''),
(77, 0, 4, 'sjhsbx', 'shhddh@g.com', '659889986446', '', 'dbdbbddbb', 'xN9JS0W8tJZViqKy', '0', '0', '', '', 0, '', 2, 3, '', '', 'dgddhhd', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 4, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_77.jpg', 'uploads/driver/driver_image_second77.jpg', 'uploads/driver/car_image_first77.jpg', 'uploads/driver/car_image_second77.jpg'),
(78, 0, 10, 'khaldoun baz', 'kkk@bbb.zzz', '9745564292929', '', 'aaappp', 'Mu6ZL6yH3nbrgFsz', '0', '0', '', '', 0, '', 5, 18, '', '', '22222', 126, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(79, 0, 4, 'pkjsjs', 'dhhdh@g.com', '89898989889898', '', 'nddnddjdj', 'U7SGBeAcu46LFjPz', '0', '0', '', '', 0, '', 2, 3, '', '', 'bsbbs', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(80, 0, 4, 'manish', 'm@gmail.com', '68686868989', '', '123456', 'DCYJZZrbFQpCGcYY', '0', '0', '', '', 0, '', 2, 3, '', '', '6h', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 4, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_80.jpg', 'uploads/driver/driver_image_second80.jpg', 'uploads/driver/car_image_first80.jpg', 'uploads/driver/car_image_second80.jpg');
INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `model_year`, `model_color`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`, `driver_image_first`, `driver_image_second`, `car_image_first`, `car_image_second`) VALUES
(81, 0, 4, 'zjjzjxjzsj', 'jdjddj@g.com', '76677999894979', '', 'znxnxnxjxjx', 'eD4H2d7jY8dDyGAH', '0', '0', '', '', 0, '', 2, 3, '', '', 'nxnxxx', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 4, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_81.jpg', 'uploads/driver/driver_image_second81.jpg', 'uploads/driver/car_image_first81.jpg', 'uploads/driver/car_image_second81.jpg'),
(82, 0, 4, 'jdjdjd', 'hdhxhxhx@g.com', '6768894677', '', 'bsbbxbxx', '8nSio0dQAYLrwErC', '0', '0', '', '', 0, '', 2, 3, '', '', 'bxbzbz', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_82.jpg', '', '', ''),
(83, 0, 4, 'hdjxhdj', 'hdhsh@g.comh', '976868698898', '', 'bxhxhhhx', 'sPKvsuA4EN8bAzvA', '0', '0', '', '', 0, '', 2, 3, '', '', 'bxbxbxb', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_83.jpg', '', '', ''),
(84, 0, 10, 'khaldoun', 'aaaa@bbbb.cccc', '97455642992', '', 'qmobile', 'rSMtq3R5EEpBOtkA', '0', '0', '', '', 0, '', 5, 18, '', '', '555854', 126, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(85, 0, 4, 'hshsshsh', 'sggsgs@g.com', '998898899898', '', 'dbdhddhdh', 'CfUSExiwWDfyUF6s', '0', '0', '', '', 0, '', 2, 3, '', '', 'hd x', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_85.jpg', 'uploads/driver/driver_image_second85.jpg', '', ''),
(86, 0, 4, 'hehsshs', 'ehsjs@g.com', '989898998989', '', '123456', 'b1RYcTZc56D7WIFe', '0', '0', '', '', 0, '', 2, 3, '', '', 'bzzzb', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(87, 0, 4, 'hbvb', 'vbb@g.com', '68868556988', '', 'fffffgggf', 'vatcE4bW2fwpWsUP', '0', '0', '', '', 0, '', 2, 3, '', '', 'gggbb', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(88, 0, 4, 'gygg', 'gyyhhg@g.com', '69898909969', '', 'vghhbbb', 'T7s3CCamLPoaiq5J', '0', '0', '', '', 0, '', 2, 3, '', '', 'vvbb', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(89, 0, 4, 'shsh', 'dshg@g.com', '9449974965', '', 'vahahssb', '3dZ77ZBziqwTgSnN', '0', '0', '', '', 0, '', 2, 3, '', '', 'bsbs', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(90, 0, 4, 'gjvb', 'chgbg@g.com', '966666665656', '', 'gggghghhh', 'BtZemgWZmmTdANpA', '0', '0', '', '', 0, '', 2, 3, '', '', 'fgghhg', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(91, 0, 4, 'nznzn', 'shhhshs@g.com', '598989889', '', 'svzvvzvzvzvz', 'ddCPkDw6uZBzREzE', '0', '0', '', '', 0, '', 2, 3, '', '', 'bxbxxx', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(92, 0, 4, 'bxbxb', 'bdhdxhx@g.com', '8998898997797', '', 'djdjjdjd', 'qmElF0RcIltk3QvO', '0', '0', '', '', 0, '', 2, 3, '', 'sjdjdd', 'vzvvz', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(93, 0, 4, 'shhshshs', 'xjjjxjx@g.com', '9898967677', '', 'hahssjjssj', 'cCJpeASQcCxs1LwA', '0', '0', '', 'dGGSIXgy-rQ:APA91bE-BYR5jn405FnrMjbBTpSsxxHWZH54d-YDr5IlYCWsUgwDdqyiHDbCkiKRxGW0bFn7viUNTVOAi6mAwZ4mMPTEPbb6k51iQ1ns4_NhMwI7tf_nw2gSEbAzY_qeHa3sbgICkA6d', 2, '', 2, 3, '87787', 'bzbbz', 'hssh', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122445', '77.0434318', '----', '05:27', 'Monday, Nov 27, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(94, 0, 4, 'hsshsshhs', 'bdbbx@g.com', '7979898989', '', 'bzbxbxbxb', '0IqGMQghx6tXMRkh', '0', '0', '', '', 0, '', 2, 3, '4959595', 'dbxbxb', 'bdxbbx', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(95, 0, 4, 'ehsh', 'shzb@g.com', '9497757777', '', 'gzgzgg', '8iWyUCiPqpw8Qf5w', '0', '0', '', '', 0, '', 2, 3, '466776', 'zhhHzz', 'zggzgz', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(96, 0, 4, 'hdhdh', 'sggsd@g.com', '89989889554', '', 'vxxxbhdhd', 'KGySko9ZRsVqVIQ2', '0', '0', '', '', 0, '', 2, 3, '65565866868', 'gdhdhhx', 'shdhdxgxx', 56, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(97, 0, 4, 'xhdhzU', 'shh@g.com', '5989898989', '', 'xbxbxxbxb', 'IIZszuT1LMmevHHD', '0', '0', '', '', 0, '', 2, 3, '989889', 'xnxnn', 'bdxbd', 56, 'Monday, Nov 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(98, 0, 4, 'whhshh', 'ehdhs@g.com', '9898989888', '', 'bzxbbxbxx', 'rPT7BIAC8jW78Ibc', '0', '0', '', '', 0, '', 2, 3, '95599898', 'dhhhddh', 'dbdbdbdb', 56, 'Monday, Nov 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(99, 0, 4, 'eyduehll', 'g@g.com', '98899899889', '', '123456', 'FQ8tYHsbuiYrrIT7', '0', '0', '', 'f63pc52HZDI:APA91bEvyyE-gi0YZuFUYtwbxtf2ORczoHY8o6mQqFeoqsMxJfoFGdJ1jz72pAQ8-93ofDLOy4SC9LtSGeqdU2Cm-07KlR_Kgn77jGjnl2lhheUqrj1qhjYRP5sWtuJpmqzsPRvdtHjl', 2, '', 2, 3, '959599', 'bbbxbb', 'dbbb', 56, 'Monday, Nov 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122129', '77.0434102', 'Not yet fetched', '07:40', 'Monday, Nov 27, 2017', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_99.jpg', 'uploads/driver/driver_image_second99.jpg', 'uploads/driver/car_image_first99.jpg', 'uploads/driver/car_image_second99.jpg'),
(100, 0, 4, 'gggg', 'q@90.com', '8800880088', '', 'qwerty', 'lORQ3fgEQLSBZ2OX', '0', '0', '', '711B40D932B03FA3E0C8F99F2CBB0479580848D82EFC706A4EDDA668EB9BB006', 1, '', 2, 3, '', '', '1234', 56, 'Monday, Nov 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121028986093', '77.0432660636278', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:22', 'Monday, Nov 27, 2017', 0, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 'uploads/driver/driver_first_image_100.jpg', 'uploads/driver/driver_image_second100.jpg', 'uploads/driver/car_image_first100.jpg', 'uploads/driver/car_image_second100.jpg'),
(101, 0, 4, 'df', 'hdshh@g.com', '99789989989', '', 'xbvxvxv', '8hDJK87cgHEw7ybR', '0', '0', '', '', 0, '', 2, 3, '555889', 'xhxhx', 'vff', 56, 'Monday, Nov 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 'uploads/driver/driver_first_image_101.jpg', '', '', ''),
(102, 0, 10, 'khaldoun baz', 'aaaa@bbbbb@ccc', '00974556429292', '', 'pass123', 'PqyorI9CA5Jc1wM6', '0', '0', '', '', 0, '', 5, 18, '2017', 'black', '665666', 126, 'Monday, Nov 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, '', '', '', ''),
(103, 0, 4, 'dghxxh', 'shhxhx@g.com', '4665598989', '', 'dvbxbb', 'FA7Q5VD4kpVR0F4j', '0', '0', '', '', 0, '', 2, 3, '5995955959', 'dhdhdhd', 'dbdbbdd', 56, 'Wednesday, Nov 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', ''),
(104, 0, 4, 'hszhz', 'dbdh@g.com', '989889898', '', 'nxjxnx', 'KLrtNrWGm6WslP0k', '0', '0', '', '', 0, '', 2, 3, '59959', 'nxxnxn', 'xzbxbx', 56, 'Wednesday, Nov 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', 'uploads/driver/car_image_second104.jpg'),
(105, 0, 4, 'dhxhhxs', 'hshsh@g.cok', '898998898998', '', 'shhzhzhz', 'plT4imamPjdT1VGQ', '0', '0', '', '', 0, '', 2, 3, '9789', 'gxgxxv', 'zvzbbx', 56, 'Wednesday, Nov 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 1, '100', 1, '96.00', '4.00', '2017-10-10'),
(2, 2, '9', 1, '8.10', '0.90', '2017-10-11'),
(3, 4, '200', 2, '190', '10', '2017-10-12'),
(4, 5, '475', 4, '456', '19', '2017-10-12'),
(5, 4, '100', 1, '95.00', '5.00', '2017-10-13'),
(6, 4, '300', 3, '285', '15', '2017-10-23'),
(7, 6, '100', 1, '96.00', '4.00', '2017-10-23'),
(8, 7, '240', 3, '216', '24', '2017-10-23'),
(9, 38, '100', 1, '96.00', '4.00', '2017-11-13'),
(10, 41, '210', 2, '201.6', '-91.6', '2017-11-14'),
(11, 42, '200', 1, '95', '5', '2017-11-15'),
(12, 43, '300', 2, '95', '5', '2017-11-15'),
(13, 44, '300', 2, '96', '4', '2017-11-16'),
(14, 44, '1420', 9, '883.2', '-163.2', '2017-11-17'),
(15, 49, '178', 1, '0.00', '0.00', '2017-11-18'),
(16, 55, '1424', 6, '1003.92', '64.08', '2017-11-21'),
(17, 66, '240', 2, '144', '16', '2017-11-21'),
(18, 49, '178', 1, '167.32', '10.68', '2017-11-22'),
(19, 55, '356', 1, '167.32', '10.68', '2017-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`) VALUES
(1, 1, 45, 1),
(2, 2, 9, 1),
(3, 4, 27, 1),
(4, 5, 65, 1),
(5, 6, 45, 1),
(6, 7, 36, 1),
(7, 36, 37, 1),
(8, 38, 37, 1),
(9, 40, 44, 1),
(10, 41, 45, 1),
(11, 42, 47, 1),
(12, 43, 56, 1),
(13, 44, 65, 1),
(14, 49, 68, 1),
(15, 55, 78, 1),
(16, 66, 77, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_charges`
--

INSERT INTO `extra_charges` (`extra_charges_id`, `city_id`, `extra_charges_type`, `extra_charges_day`, `slot_one_starttime`, `slot_one_endtime`, `slot_two_starttime`, `slot_two_endtime`, `payment_type`, `slot_price`) VALUES
(1, 126, 1, 'Wednesday', '05:00 PM', '06:04 PM', '07:04 PM', '08:04 PM', 1, '20'),
(2, 126, 2, 'Night', '07:00 PM', '06:59 AM', '', '', 1, '10');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 2, '', '4.603615850142076', '-74.21616721898317', 'null', '4.7068378', '-74.0556746', 'Calle 127', 'Tuesday, Oct 10', '15:05:30', '03:05:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.603615850142076,-74.21616721898317&markers=color:red|label:D|4.7068378,-74.0556746&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(2, 2, '', '4.603625541781475', '-74.21615548431873', 'null', '', '', 'Configurarsupuntodeentrega', 'Tuesday, Oct 10', '15:06:51', '03:06:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.603625541781475,-74.21615548431873&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(3, 2, '', '4.603682354837335', '-74.21608675271273', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '4.6908901', '-74.0658158', 'Suba - Calle 100 B - 4', 'Tuesday, Oct 10', '16:22:52', '04:22:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.603682354837335,-74.21608675271273&markers=color:red|label:D|4.6908901,-74.0658158&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(4, 2, '', '4.603681352254033', '-74.21604115515947', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '4.740881', '-74.08381', 'Suba', 'Tuesday, Oct 10', '16:23:28', '04:23:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.603681352254033,-74.21604115515947&markers=color:red|label:D|4.740881,-74.08381&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(5, 2, '', '4.603612842391892', '-74.21602942049502', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '', '', 'Configurarsupuntodeentrega', 'Tuesday, Oct 10', '16:24:04', '04:24:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.603612842391892,-74.21602942049502&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(6, 2, '', '4.60362353661472', '-74.2160639539361', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '4.6395037', '-74.1401447', 'Castilla', 'Tuesday, Oct 10', '16:24:59', '04:24:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.60362353661472,-74.2160639539361&markers=color:red|label:D|4.6395037,-74.1401447&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 3, 0, 1),
(7, 2, '', '4.60362353661472', '-74.2160639539361', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '4.740881', '-74.08381', 'Suba', 'Tuesday, Oct 10', '16:25:33', '04:25:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.60362353661472,-74.2160639539361&markers=color:red|label:D|4.740881,-74.08381&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(8, 2, '', '4.616619572001229', '-74.09216467291117', 'Cra. 32 #13-52, BogotÃ¡, Colombia', '4.6908901', '-74.0658158', 'Suba - Calle 100 B - 4', 'Tuesday, Oct 10', '20:07:50', '08:07:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.616619572001229,-74.09216467291117&markers=color:red|label:D|4.6908901,-74.0658158&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(9, 2, '', '4.603574075832824', '-74.21602942049502', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '4.740881', '-74.08381', 'Suba', '', '07:16:53', '07:16:53 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|4.603574075832824,-74.21602942049502&markers=color:red|label:D|4.740881,-74.08381&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(10, 8, '', '25.350527780905892', '51.502453163266175', 'Al magta, Doha, Qatar', '25.333239404302685', '51.51473868638277', 'Ibn Toloun St, Doha, Qatar', '', '18:09:21', '06:09:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.350527780905892,51.502453163266175&markers=color:red|label:D|25.333239404302685,51.51473868638277&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(11, 8, '', '25.350527780905892', '51.502453163266175', 'Al magta, Doha, Qatar', '25.333239404302685', '51.51473868638277', 'Ibn Toloun St, Doha, Qatar', '', '18:09:42', '06:09:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.350527780905892,51.502453163266175&markers=color:red|label:D|25.333239404302685,51.51473868638277&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(12, 8, '', '25.350545960353724', '51.50263823568821', 'Al magta, Doha, Qatar', '', '', 'Set your drop point', '', '20:30:01', '08:30:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.350545960353724,51.50263823568821&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(13, 8, '', '25.325840600115928', '51.526348292827606', 'Al Reem Tower, Conference Centre, Doha, Qatar', '', '', 'Set your drop point', '', '11:27:39', '11:27:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.325840600115928,51.526348292827606&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(14, 8, '', '25.32583605432619', '51.52634359896183', 'Al Reem Tower, Conference Centre, Doha, Qatar', '', '', 'Set your drop point', '', '11:27:54', '11:27:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.32583605432619,51.52634359896183&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(15, 8, '', '25.325845448958127', '51.52635097503662', 'Al Reem Tower, Conference Centre, Doha, Qatar', '', '', 'Set your drop point', '', '11:28:04', '11:28:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.325845448958127,51.52635097503662&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(16, 8, '', '25.325882421373528', '51.52638617902994', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', '', '', 'Set your drop point', '', '11:42:45', '11:42:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.325882421373528,51.52638617902994&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(17, 8, '', '25.350366286691', '51.50253798812628', 'Al magta, Doha, Qatar', '25.2912323', '51.53232860000001', 'Souq Waqif', '', '08:13:55', '08:13:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.350366286691,51.50253798812628&markers=color:red|label:D|25.2912323,51.53232860000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(18, 8, '', '25.325848176431776', '51.526362374424934', 'Al Reem Tower, Conference Centre, Doha, Qatar', '', '', 'Set your drop point', '', '09:02:42', '09:02:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.325848176431776,51.526362374424934&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 4, 0, 1),
(19, 8, '', '25.325962427217387', '51.526918932795525', 'Omar Al Mukhtar St, Doha, Qatar', '', '', 'Set your drop point', '', '13:29:12', '01:29:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.325962427217387,51.526918932795525&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(20, 8, '', '25.325990914133882', '51.52674224227666', 'Al Reem Tower, Conference Centre, Doha, Qatar', '', '', 'Set your drop point', '', '13:38:44', '01:38:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|25.325990914133882,51.52674224227666&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(21, 8, '', '25.350273268349287', '51.502520218491554', 'Al magta, Doha, Qatar', '', '', 'Set your drop point', 'Thursday, Nov 23', '11:56:31', '11:56:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.350273268349287,51.502520218491554&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 5, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', 'About Us and other information about company will go here.', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 1, '1', 'Cash', 'Admin', '100', 'Tuesday, Oct 10', '1'),
(2, 5, 2, '1', 'Cash', 'Admin', '9', 'Wednesday, Oct 11', '1'),
(3, 6, 4, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 12', '1'),
(4, 7, 4, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 12', '1'),
(5, 8, 5, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 12', '1'),
(6, 9, 5, '1', 'Cash', 'Admin', '175', 'Thursday, Oct 12', '1'),
(7, 10, 5, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 12', '1'),
(8, 11, 5, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 12', '1'),
(9, 12, 4, '1', 'Cash', 'Admin', '100', 'Friday, Oct 13', '1'),
(10, 13, 4, '1', 'Cash', 'Admin', '100', 'Monday, Oct 23', '1'),
(11, 14, 4, '1', 'Cash', 'Admin', '100', 'Monday, Oct 23', '1'),
(12, 15, 4, '1', 'Cash', 'Admin', '100', 'Monday, Oct 23', '1'),
(13, 16, 6, '1', 'Cash', 'Admin', '100', 'Monday, Oct 23', '1'),
(14, 17, 7, '1', 'Cash', 'Admin', '80', 'Monday, Oct 23', '1'),
(15, 19, 8, '1', 'Cash', 'Admin', '80', 'Monday, Oct 23', '1'),
(16, 20, 8, '1', 'Cash', 'Admin', '80', 'Monday, Oct 23', '1'),
(17, 21, 9, '1', 'Cash', 'Admin', '100', 'Monday, Nov 13', '1'),
(18, 22, 12, '1', 'Cash', 'Admin', '110', 'Tuesday, Nov 14', '1'),
(19, 23, 12, '4', 'Cash', 'Android', '57', 'Anything', 'Anything'),
(20, 24, 13, '1', 'Cash', 'Ios', '100', '15 November 2017', 'Done'),
(21, 25, 14, '1', 'Cash', 'Ios', '100', '15 November 2017', 'Done'),
(22, 28, 5, '2', 'Cash', 'Android', '100', 'Anything', 'Anything'),
(23, 30, 5, '1', 'Cash', 'Admin', '110', 'Friday, Nov 17', '1'),
(24, 30, 5, '1', 'Cash', 'Admin', '110', 'Friday, Nov 17', '1'),
(25, 31, 5, '2', 'noQoody', 'Android', '100', 'Anything', 'Anything'),
(26, 32, 5, '1', 'Cash', 'Admin', '100', 'Friday, Nov 17', '1'),
(27, 33, 5, '2', 'noQoody', 'Android', '100', 'Anything', 'Anything'),
(28, 29, 5, '2', 'Cash', 'Android', '100', 'Anything', 'Anything'),
(29, 34, 5, '2', 'Cash', 'Android', '100', 'Anything', 'Anything'),
(30, 35, 5, '1', 'Cash', 'Admin', '100', 'Friday, Nov 17', '1'),
(31, 36, 5, '2', 'Cash', 'Android', '100', 'Anything', 'Anything'),
(32, 38, 19, '1', 'Cash', 'Admin', '178', 'Tuesday, Nov 21', '1'),
(33, 39, 19, '1', 'Cash', 'Ios', '178', '21 November 2017', 'Done'),
(34, 40, 19, '1', 'Cash', 'Ios', '178', '21 November 2017', 'Done'),
(35, 41, 19, '1', 'Cash', 'Admin', '178', 'Tuesday, Nov 21', '1'),
(36, 42, 19, '1', 'Cash', 'Admin', '178', 'Tuesday, Nov 21', '1'),
(37, 43, 19, '1', 'Cash', 'Admin', '178', 'Tuesday, Nov 21', '1'),
(38, 44, 8, '2', 'Cash', 'Android', '80', 'Anything', 'Anything'),
(39, 45, 8, '1', 'Cash', 'Admin', '80', 'Tuesday, Nov 21', '1'),
(40, 37, 13, '1', 'Cash', 'Ios', '178', '22 November 2017', 'Done'),
(41, 46, 19, '1', 'Cash', 'Ios', '178', '22 November 2017', 'Done');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'NoQoody', '', '', 1),
(3, 'Credit Card', '', '', 2),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(3, 56, 'Miles', '', 1, 5, 0, 0, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(12, 56, 'Miles', '', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 'Miles', '', 5, 7, 0, 0, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(51, 124, 'Km', 'COP', 3, 10, 0, 0, 0, 0, 0, 0, '3', '9', '10', '0', '0', '0', '0'),
(50, 124, 'Km', 'COP', 2, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0'),
(52, 126, 'Km', 'QAR', 2, 10, 0, 0, 0, 0, 0, 0, '3', '80', '10', '0', '0', '0', '0'),
(53, 126, 'Km', 'QAR', 3, 10, 0, 0, 0, 0, 0, 0, '3', '90', '10', '0', '0', '0', '0'),
(54, 126, 'Km', 'QAR', 5, 10, 0, 0, 0, 0, 0, 0, '1', '5', '3', '1', '1', '5', '2'),
(55, 126, 'Km', 'QAR', 4, 20, 0, 0, 0, 0, 0, 0, '1', '10', '5', '1', '3', '5', '1');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'hola maria', 'saludos maria aqui esta el fuec', 'uploads/notification/1500380956793.jpg', '', 0, 2, '2017-10-11', 2),
(2, 'Fuec Solicitado', 'Fuec Registro  X903', 'uploads/notification/1500380956793.jpg', '', 0, 2, '2017-10-11', 2),
(3, 'hi', 'welcome', 'uploads/notification/1500380956793.jpg', '', 0, 0, '2017-11-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 1, 1, 0, '2017-10-10'),
(2, 2, 1, 0, '2017-10-10'),
(3, 3, 1, 1, '2017-10-10'),
(4, 4, 1, 1, '2017-10-10'),
(5, 5, 1, 1, '2017-10-10'),
(6, 6, 2, 1, '2017-10-10'),
(7, 7, 2, 1, '2017-10-11'),
(8, 8, 2, 0, '2017-10-11'),
(9, 9, 2, 0, '2017-10-11'),
(10, 10, 4, 1, '2017-10-12'),
(11, 11, 4, 1, '2017-10-12'),
(12, 12, 4, 1, '2017-10-12'),
(13, 13, 4, 1, '2017-10-12'),
(14, 14, 1, 0, '2017-10-12'),
(15, 15, 1, 0, '2017-10-12'),
(16, 15, 5, 1, '2017-10-12'),
(17, 16, 1, 0, '2017-10-12'),
(18, 16, 5, 1, '2017-10-12'),
(19, 17, 1, 0, '2017-10-12'),
(20, 17, 5, 1, '2017-10-12'),
(21, 18, 1, 0, '2017-10-12'),
(22, 18, 5, 1, '2017-10-12'),
(23, 19, 1, 0, '2017-10-13'),
(24, 19, 5, 0, '2017-10-13'),
(25, 19, 1, 0, '0000-00-00'),
(26, 20, 4, 1, '2017-10-13'),
(27, 21, 1, 0, '2017-10-17'),
(28, 21, 5, 0, '2017-10-17'),
(29, 22, 1, 0, '2017-10-17'),
(30, 22, 5, 0, '2017-10-17'),
(31, 23, 4, 0, '2017-10-23'),
(32, 24, 4, 0, '2017-10-23'),
(33, 25, 4, 1, '2017-10-23'),
(34, 26, 4, 1, '2017-10-23'),
(35, 27, 4, 1, '2017-10-23'),
(36, 28, 1, 0, '2017-10-23'),
(37, 28, 5, 0, '2017-10-23'),
(38, 28, 6, 1, '2017-10-23'),
(39, 29, 7, 1, '2017-10-23'),
(40, 30, 7, 1, '2017-10-23'),
(41, 31, 7, 1, '2017-10-23'),
(42, 32, 7, 0, '2017-10-23'),
(43, 33, 7, 1, '2017-10-23'),
(44, 34, 7, 0, '2017-10-26'),
(45, 35, 7, 0, '2017-10-29'),
(46, 36, 7, 0, '2017-11-01'),
(47, 37, 1, 0, '2017-11-13'),
(48, 37, 5, 0, '2017-11-13'),
(49, 37, 6, 0, '2017-11-13'),
(50, 37, 36, 0, '2017-11-13'),
(51, 37, 38, 1, '2017-11-13'),
(52, 38, 1, 0, '2017-11-14'),
(53, 38, 5, 0, '2017-11-14'),
(54, 38, 6, 0, '2017-11-14'),
(55, 38, 40, 0, '2017-11-14'),
(56, 38, 41, 1, '2017-11-14'),
(57, 39, 1, 0, '2017-11-14'),
(58, 39, 5, 0, '2017-11-14'),
(59, 39, 6, 0, '2017-11-14'),
(60, 39, 40, 0, '2017-11-14'),
(61, 39, 41, 1, '2017-11-14'),
(62, 40, 1, 0, '2017-11-14'),
(63, 40, 5, 0, '2017-11-14'),
(64, 40, 6, 0, '2017-11-14'),
(65, 40, 40, 0, '2017-11-14'),
(66, 40, 41, 0, '2017-11-14'),
(67, 40, 6, 0, '0000-00-00'),
(68, 41, 1, 0, '2017-11-14'),
(69, 41, 5, 0, '2017-11-14'),
(70, 41, 6, 0, '2017-11-14'),
(71, 41, 40, 0, '2017-11-14'),
(72, 41, 41, 0, '2017-11-14'),
(73, 42, 1, 0, '2017-11-14'),
(74, 42, 5, 0, '2017-11-14'),
(75, 42, 6, 0, '2017-11-14'),
(76, 42, 40, 0, '2017-11-14'),
(77, 42, 41, 0, '2017-11-14'),
(78, 43, 1, 0, '2017-11-14'),
(79, 43, 5, 0, '2017-11-14'),
(80, 43, 6, 0, '2017-11-14'),
(81, 43, 40, 0, '2017-11-14'),
(82, 43, 41, 0, '2017-11-14'),
(83, 44, 1, 0, '2017-11-14'),
(84, 44, 5, 0, '2017-11-14'),
(85, 44, 6, 0, '2017-11-14'),
(86, 44, 40, 0, '2017-11-14'),
(87, 44, 41, 1, '2017-11-14'),
(88, 45, 1, 0, '2017-11-14'),
(89, 45, 5, 0, '2017-11-14'),
(90, 45, 6, 0, '2017-11-14'),
(91, 45, 41, 1, '2017-11-14'),
(92, 46, 42, 1, '2017-11-15'),
(93, 47, 42, 1, '2017-11-15'),
(94, 48, 43, 0, '2017-11-15'),
(95, 49, 43, 0, '2017-11-15'),
(96, 50, 43, 1, '2017-11-15'),
(97, 51, 43, 0, '2017-11-15'),
(98, 52, 43, 0, '2017-11-15'),
(99, 53, 43, 0, '2017-11-15'),
(100, 54, 43, 0, '2017-11-15'),
(101, 55, 43, 0, '2017-11-15'),
(102, 56, 43, 1, '2017-11-15'),
(103, 57, 5, 0, '2017-11-16'),
(104, 57, 44, 1, '2017-11-16'),
(105, 58, 5, 0, '2017-11-17'),
(106, 58, 44, 1, '2017-11-17'),
(107, 59, 5, 0, '2017-11-17'),
(108, 59, 44, 1, '2017-11-17'),
(109, 60, 5, 0, '2017-11-17'),
(110, 60, 44, 1, '2017-11-17'),
(111, 61, 5, 0, '2017-11-17'),
(112, 61, 44, 1, '2017-11-17'),
(113, 62, 5, 0, '2017-11-17'),
(114, 62, 44, 1, '2017-11-17'),
(115, 63, 5, 0, '2017-11-17'),
(116, 63, 44, 1, '2017-11-17'),
(117, 64, 5, 0, '2017-11-17'),
(118, 64, 44, 1, '2017-11-17'),
(119, 65, 5, 0, '2017-11-17'),
(120, 65, 44, 1, '2017-11-17'),
(121, 66, 49, 1, '2017-11-18'),
(122, 67, 49, 0, '2017-11-21'),
(123, 67, 55, 1, '2017-11-21'),
(124, 68, 49, 0, '2017-11-21'),
(125, 68, 55, 1, '2017-11-21'),
(126, 69, 55, 1, '2017-11-21'),
(127, 70, 55, 0, '2017-11-21'),
(128, 71, 55, 1, '2017-11-21'),
(129, 72, 55, 0, '2017-11-21'),
(130, 73, 55, 1, '2017-11-21'),
(131, 74, 55, 1, '2017-11-21'),
(132, 75, 66, 0, '2017-11-21'),
(133, 76, 66, 1, '2017-11-21'),
(134, 77, 66, 1, '2017-11-21'),
(135, 78, 55, 1, '2017-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 8, 2),
(2, 8, 2),
(3, 9, 2),
(4, 9, 2),
(5, 19, 5),
(6, 40, 41),
(7, 70, 55);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 1, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '12:12:00', '12:12:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-10'),
(2, 1, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '12:15:06', '12:15:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-10'),
(3, 1, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '12:18:02', '12:19:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(4, 1, '', '28.41473830764645', '77.04614147543907', 'Tower 22, South City II, Sector 49, Gurugram, Haryana 122018, India', '28.632862', '77.2195421', 'Rajiv Chowk', 'Tuesday, Oct 10', '13:05:21', '01:06:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41473830764645,77.04614147543907&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-10'),
(5, 1, '', '28.41473830764645', '77.04614147543907', 'Tower 22, South City II, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '13:07:08', '01:07:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41473830764645,77.04614147543907&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-10'),
(6, 2, '', '4.616658003660217', '-74.09214187413454', 'Cra. 32 #13-52, BogotÃ¡, Colombia', '4.7068378', '-74.0556746', 'Calle 127', 'Tuesday, Oct 10', '20:08:32', '08:21:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.616658003660217,-74.09214187413454&markers=color:red|label:D|4.7068378,-74.0556746&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-10'),
(7, 2, '', '4.603574075832824', '-74.21602942049502', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '4.740881', '-74.08381', 'Suba', 'Wednesday, Oct 11', '05:21:17', '05:23:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.603574075832824,-74.21602942049502&markers=color:red|label:D|4.740881,-74.08381&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(8, 2, '', '4.603574075832824', '-74.21602942049502', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '4.740881', '-74.08381', 'Suba', 'Wednesday, Oct 11', '06:52:14', '06:52:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.603574075832824,-74.21602942049502&markers=color:red|label:D|4.740881,-74.08381&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-11'),
(9, 2, '', '4.603574075832824', '-74.21602942049502', 'Av Potrero Grande, Soacha, Cundinamarca, Colombia', '4.740881', '-74.08381', 'Suba', 'Wednesday, Oct 11', '07:11:10', '07:11:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.603574075832824,-74.21602942049502&markers=color:red|label:D|4.740881,-74.08381&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-11'),
(10, 6, '', '28.4121007064694', '77.043224948579', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '12:08:05', '12:08:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121007064694,77.043224948579&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-12'),
(11, 6, '', '28.4121007064694', '77.043224948579', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '12:09:23', '12:09:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121007064694,77.043224948579&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-12'),
(12, 4, '', '28.4121636429489', '77.043334543705', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '12:56:54', '12:57:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121636429489,77.043334543705&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(13, 4, '', '28.412188016836', '77.0432244893881', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '14:12:37', '02:14:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412188016836,77.0432244893881&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(14, 5, '', '28.412169835666127', '77.04318970441818', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '15:12:59', '03:12:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412169835666127,77.04318970441818&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-12'),
(15, 5, '', '28.412169835666127', '77.04318970441818', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '15:14:38', '03:15:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412169835666127,77.04318970441818&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(16, 5, '', '28.412169835666127', '77.04318970441818', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '15:15:28', '03:21:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412169835666127,77.04318970441818&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(17, 5, '', '28.412169835666127', '77.04318970441818', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '15:36:28', '03:36:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412169835666127,77.04318970441818&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(18, 5, '', '28.434298524523093', '77.03520141541956', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 12', '20:09:45', '08:10:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.434298524523093,77.03520141541956&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(19, 4, '', '28.4105308172012', '77.0433241501451', '71, Shanti Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 13', '14:08:04', '02:08:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4105308172012,77.0433241501451&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(20, 4, '', '28.4105308172012', '77.0433241501451', '71, Shanti Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 13', '14:08:34', '02:10:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4105308172012,77.0433241501451&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-13'),
(21, 4, '', '28.4121083850048', '77.0432517330982', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4491822474172', '77.1144600212574', 'Unnamed Road, DLF Phase 5, Sector 54\nGurugram, Haryana 122011', 'Tuesday, Oct 17', '11:43:06', '11:43:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121083850048,77.0432517330982&markers=color:red|label:D|28.4491822474172,77.1144600212574&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(22, 4, '', '28.4121083850048', '77.0432517330982', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4491822474172', '77.1144600212574', 'Unnamed Road, DLF Phase 5, Sector 54\nGurugram, Haryana 122011', 'Tuesday, Oct 17', '11:44:13', '11:44:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121083850048,77.0432517330982&markers=color:red|label:D|28.4491822474172,77.1144600212574&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(23, 4, '', '28.4120996515162', '77.0433026924729', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4311461589072', '77.0640710368752', '222, Sector 51\nGurugram, Haryana 122022', 'Monday, Oct 23', '06:50:06', '06:50:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120996515162,77.0433026924729&markers=color:red|label:D|28.4311461589072,77.0640710368752&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(24, 4, '', '28.412095648263', '77.0433336217674', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.429824684782', '77.0853942632675', 'H62, Manohara Marg, Block B1, Sector 52\nGurugram, Haryana 122011', 'Monday, Oct 23', '06:52:05', '06:52:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412095648263,77.0433336217674&markers=color:red|label:D|28.429824684782,77.0853942632675&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(25, 4, '', '28.4120972923843', '77.0432171970606', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4412497897572', '77.1064596623182', 'Suncity, Sector 54\nGurugram, Haryana', 'Monday, Oct 23', '07:21:15', '07:21:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120972923843,77.0432171970606&markers=color:red|label:D|28.4412497897572,77.1064596623182&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(26, 4, '', '28.4120970743143', '77.0432421159316', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4435769965377', '77.1125603467226', 'Sector 54\nGurugram, Haryana', 'Monday, Oct 23', '07:31:08', '07:31:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120970743143,77.0432421159316&markers=color:red|label:D|28.4435769965377,77.1125603467226&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(27, 4, '', '28.4120910504289', '77.0433905060817', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4526182425489', '77.1293218061328', 'Block E, Aya Nagar Extension, Aya Nagar\nNew Delhi, Delhi', 'Monday, Oct 23', '07:44:57', '07:45:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120910504289,77.0433905060817&markers=color:red|label:D|28.4526182425489,77.1293218061328&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(28, 6, '', '28.40988115964654', '77.0428279414773', 'P-103, Sohna Rd, Parsvnath Greenville, Tatvam Villas, Uppal Southend, Sector 48, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 23', '08:09:33', '08:11:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.40988115964654,77.0428279414773&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(29, 7, '', '25.32589514957951', '51.52632247656584', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', '25.238806500000003', '51.4913579', 'Abu Hamour', 'Monday, Oct 23', '15:45:52', '03:47:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.32589514957951,51.52632247656584&markers=color:red|label:D|25.238806500000003,51.4913579&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(30, 8, '', '25.350566866715347', '51.502453163266175', 'Al magta, Doha, Qatar', '', '', 'Set your drop point', 'Monday, Oct 23', '17:09:40', '06:09:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.350566866715347,51.502453163266175&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-23'),
(31, 8, '', '25.350527780905892', '51.502453163266175', 'Al magta, Doha, Qatar', '25.333239404302685', '51.51473868638277', 'Ibn Toloun St, Doha, Qatar', 'Monday, Oct 23', '18:09:52', '06:11:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.350527780905892,51.502453163266175&markers=color:red|label:D|25.333239404302685,51.51473868638277&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(32, 8, '', '25.350527780905892', '51.502453163266175', 'Al magta, Doha, Qatar', '25.333239404302685', '51.51473868638277', 'Ibn Toloun St, Doha, Qatar', 'Monday, Oct 23', '18:12:05', '06:12:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.350527780905892,51.502453163266175&markers=color:red|label:D|25.333239404302685,51.51473868638277&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(33, 8, '', '25.350527780905892', '51.502453163266175', 'Al magta, Doha, Qatar', '25.333239404302685', '51.51473868638277', 'Ibn Toloun St, Doha, Qatar', 'Monday, Oct 23', '18:12:17', '06:14:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.350527780905892,51.502453163266175&markers=color:red|label:D|25.333239404302685,51.51473868638277&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(34, 8, '', '25.32591818156786', '51.52637377381324', 'Al Reem / Tatweer Tower, Al Wehda St, Doha, Qatar', '', '', 'Set your drop point', 'Thursday, Oct 26', '13:43:55', '01:43:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.32591818156786,51.52637377381324&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-26'),
(35, 8, '', '25.350590196998716', '51.50239985436201', 'Al magta, Doha, Qatar', '', '', 'Set your drop point', 'Sunday, Oct 29', '01:02:23', '01:02:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.350590196998716,51.50239985436201&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-29'),
(36, 8, '', '25.350622616995363', '51.502353586256504', 'Al magta, Doha, Qatar', '', '', 'Set your drop point', 'Wednesday, Nov 1', '17:18:28', '05:18:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.350622616995363,51.502353586256504&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(37, 9, '', '28.4121742590354', '77.0432805642486', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4345175827294', '77.0837742090225', 'Wazirabad Road, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Monday, Nov 13', '13:19:37', '01:19:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121742590354,77.0432805642486&markers=color:red|label:D|28.4345175827294,77.0837742090225&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 38, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-13'),
(38, 12, '', '28.41233998780311', '77.04345222562551', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.44722454103309', '77.0800519734621', '1707, St Thomas Marg, Sector 43, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '13:27:39', '01:30:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233998780311,77.04345222562551&markers=color:red|label:D|28.44722454103309,77.0800519734621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 41, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(39, 12, '', '28.41233998780311', '77.04345222562551', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.44722454103309', '77.0800519734621', '1707, St Thomas Marg, Sector 43, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '13:32:09', '01:32:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233998780311,77.04345222562551&markers=color:red|label:D|28.44722454103309,77.0800519734621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 41, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-14'),
(40, 12, '', '28.41233998780311', '77.04345222562551', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.44722454103309', '77.0800519734621', '1707, St Thomas Marg, Sector 43, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '13:32:35', '01:32:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233998780311,77.04345222562551&markers=color:red|label:D|28.44722454103309,77.0800519734621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(41, 12, '', '28.41233998780311', '77.04345222562551', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.44722454103309', '77.0800519734621', '1707, St Thomas Marg, Sector 43, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '13:33:02', '01:33:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233998780311,77.04345222562551&markers=color:red|label:D|28.44722454103309,77.0800519734621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(42, 12, '', '28.41233998780311', '77.04345222562551', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.44722454103309', '77.0800519734621', '1707, St Thomas Marg, Sector 43, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '13:33:59', '01:33:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233998780311,77.04345222562551&markers=color:red|label:D|28.44722454103309,77.0800519734621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(43, 12, '', '28.41233998780311', '77.04345222562551', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.44722454103309', '77.0800519734621', '1707, St Thomas Marg, Sector 43, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '13:34:18', '01:34:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233998780311,77.04345222562551&markers=color:red|label:D|28.44722454103309,77.0800519734621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(44, 12, '', '28.41233998780311', '77.04345222562551', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.44722454103309', '77.0800519734621', '1707, St Thomas Marg, Sector 43, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '13:34:32', '01:34:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233998780311,77.04345222562551&markers=color:red|label:D|28.44722454103309,77.0800519734621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 41, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(45, 12, '', '28.41233998780311', '77.04345222562551', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.44722454103309', '77.0800519734621', '1707, St Thomas Marg, Sector 43, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '13:36:52', '01:37:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233998780311,77.04345222562551&markers=color:red|label:D|28.44722454103309,77.0800519734621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 41, 2, 1, 1, 7, 1, 0, 4, 0, 1, 1, '2017-11-14'),
(46, 13, '', '28.4120645594237', '77.0434371381998', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4378225646933', '77.083593159914', '1438, Sarswati Kunj II, Wazirabad, Sector 52\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '09:32:25', '09:33:47 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120645594237,77.0434371381998&markers=color:red|label:D|28.4378225646933,77.083593159914&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 42, 3, 1, 1, 2, 0, 13, 1, 0, 1, 1, '2017-11-15'),
(47, 13, '', '28.4120985819291', '77.0433053747537', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4378225646933', '77.083593159914', '1438, Sarswati Kunj II, Wazirabad, Sector 52\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '09:34:33', '09:35:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120985819291,77.0433053747537&markers=color:red|label:D|28.4378225646933,77.083593159914&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 42, 3, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-15'),
(48, 14, '', '28.4121288878965', '77.0432956382053', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4203462656687', '77.0750141143799', 'E 142, Sushant Lok III Extension, Sector 57\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '10:02:09', '10:02:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121288878965,77.0432956382053&markers=color:red|label:D|28.4203462656687,77.0750141143799&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-15'),
(49, 14, '', '28.4121288878965', '77.0432956382053', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4203462656687', '77.0750141143799', 'E 142, Sushant Lok III Extension, Sector 57\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '10:03:24', '10:03:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121288878965,77.0432956382053&markers=color:red|label:D|28.4203462656687,77.0750141143799&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-15'),
(50, 14, '', '28.4121288878965', '77.0432956382053', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4203462656687', '77.0750141143799', 'E 142, Sushant Lok III Extension, Sector 57\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '10:05:16', '10:05:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121288878965,77.0432956382053&markers=color:red|label:D|28.4203462656687,77.0750141143799&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 43, 3, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-15'),
(51, 14, '', '28.4120648543153', '77.0432296022773', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4512174453855', '77.0865114033222', '2129, Sushant Lok Road, Block C, Sushant Lok Phase I, Sector 43\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '10:17:55', '10:17:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120648543153,77.0432296022773&markers=color:red|label:D|28.4512174453855,77.0865114033222&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-15'),
(52, 14, '', '28.4120648543153', '77.0432296022773', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4512174453855', '77.0865114033222', '2129, Sushant Lok Road, Block C, Sushant Lok Phase I, Sector 43\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '10:19:13', '10:19:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120648543153,77.0432296022773&markers=color:red|label:D|28.4512174453855,77.0865114033222&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-15'),
(53, 14, '', '28.4120648543153', '77.0432296022773', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4512174453855', '77.0865114033222', '2129, Sushant Lok Road, Block C, Sushant Lok Phase I, Sector 43\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '10:20:24', '10:20:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120648543153,77.0432296022773&markers=color:red|label:D|28.4512174453855,77.0865114033222&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-15'),
(54, 14, '', '28.4120648543153', '77.0432296022773', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4512174453855', '77.0865114033222', '2129, Sushant Lok Road, Block C, Sushant Lok Phase I, Sector 43\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '10:21:12', '10:21:12 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120648543153,77.0432296022773&markers=color:red|label:D|28.4512174453855,77.0865114033222&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-15'),
(55, 14, '', '28.4120648543153', '77.0432296022773', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4512174453855', '77.0865114033222', '2129, Sushant Lok Road, Block C, Sushant Lok Phase I, Sector 43\nGurugram, Haryana 122003', 'Wednesday, Nov 15', '10:22:21', '10:22:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120648543153,77.0432296022773&markers=color:red|label:D|28.4512174453855,77.0865114033222&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 2, 0, 1, 1, '2017-11-15'),
(56, 14, '', '28.4121106518697', '77.0432437677654', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4276139354879', '77.0858120173216', '2016, Manohara Marg, Block B1, Sector 57\nGurugram, Haryana 122011', 'Wednesday, Nov 15', '10:23:12', '10:23:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121106518697,77.0432437677654&markers=color:red|label:D|28.4276139354879,77.0858120173216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 43, 3, 1, 1, 7, 0, 0, 2, 0, 1, 1, '2017-11-15'),
(57, 5, '', '28.434647602257762', '77.03503746539354', '133, Sohna Rd, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 16', '18:36:01', '06:36:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.434647602257762,77.03503746539354&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-16'),
(58, 5, '', '28.41232553815219', '77.04344484955072', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '05:05:32', '05:05:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232553815219,77.04344484955072&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-17'),
(59, 5, '', '28.412319640334914', '77.04343680292368', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '06:08:09', '06:10:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412319640334914,77.04343680292368&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(60, 5, '', '28.412319640334914', '77.04343680292368', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '06:14:45', '06:15:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412319640334914,77.04343680292368&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-17'),
(61, 5, '', '28.412333205314145', '77.0434371381998', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '07:08:10', '07:08:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412333205314145,77.0434371381998&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(62, 5, '', '28.412333205314145', '77.0434371381998', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '07:08:42', '07:08:53 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412333205314145,77.0434371381998&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-17'),
(63, 5, '', '28.412327307497304', '77.04344853758812', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '07:23:38', '07:23:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412327307497304,77.04344853758812&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-17'),
(64, 5, '', '28.41233762867655', '77.04343345016241', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '09:11:18', '09:11:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233762867655,77.04343345016241&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(65, 5, '', '28.41233762867655', '77.04343345016241', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '09:12:10', '09:12:33 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233762867655,77.04343345016241&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 2, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-17'),
(66, 13, '', '28.4121364681315', '77.0432776306542', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.447078618085', '77.1142695844173', 'Sector 54\nGurugram, Haryana', 'Saturday, Nov 18', '10:41:54', '10:42:17 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121364681315,77.0432776306542&markers=color:red|label:D|28.447078618085,77.1142695844173&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 49, 4, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-18'),
(67, 19, '', '28.4120477506022', '77.0432560890913', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4236354729172', '77.0752581954002', 'Dharm Marg, Rail Vihar, Sector 57\nGurugram, Haryana 122413', 'Tuesday, Nov 21', '06:45:16', '06:45:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120477506022,77.0432560890913&markers=color:red|label:D|28.4236354729172,77.0752581954002&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 55, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(68, 19, '', '28.4120784653615', '77.0431849268051', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4190741952308', '77.0775890350342', 'Service Lane, Block C, Sushant Lok III, Sector 57\nGurugram, Haryana 122413', 'Tuesday, Nov 21', '06:55:46', '06:56:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120784653615,77.0431849268051&markers=color:red|label:D|28.4190741952308,77.0775890350342&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 55, 4, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-21'),
(69, 19, '', '28.4121524370787', '77.043255418539', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4276407666777', '77.0850428938866', '1920, Sushant Lok Phase 3, Block B1, Sector 57\nGurugram, Haryana 122413', 'Tuesday, Nov 21', '07:21:03', '07:21:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121524370787,77.043255418539&markers=color:red|label:D|28.4276407666777,77.0850428938866&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 55, 4, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-21'),
(70, 19, '', '28.4121019765999', '77.0432841685387', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4331141871748', '77.0613683760166', '1624, Sector Main Road, Huda Colony, Sector 46\nGurugram, Haryana 122018', 'Tuesday, Nov 21', '09:06:26', '09:06:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121019765999,77.0432841685387&markers=color:red|label:D|28.4331141871748,77.0613683760166&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 55, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-21'),
(71, 19, '', '28.4121019765999', '77.0432841685387', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4331141871748', '77.0613683760166', '1624, Sector Main Road, Huda Colony, Sector 46\nGurugram, Haryana 122018', 'Tuesday, Nov 21', '09:07:22', '09:08:11 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121019765999,77.0432841685387&markers=color:red|label:D|28.4331141871748,77.0613683760166&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 55, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(72, 19, '', '28.4121033571887', '77.0433017959507', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Tuesday, Nov 21', '09:20:00', '09:20:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121033571887,77.0433017959507&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-21'),
(73, 19, '', '28.4122133592067', '77.0432778151546', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4629809709408', '77.0954398065805', 'Golf Course Road Underpass\nGurugram, Haryana 122022', 'Tuesday, Nov 21', '10:19:59', '10:21:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122133592067,77.0432778151546&markers=color:red|label:D|28.4629809709408,77.0954398065805&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 55, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(74, 19, '', '28.4121845969148', '77.0432331677059', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.45908', '77.070518', 'Huda City Metro Station', 'Tuesday, Nov 21', '10:31:34', '10:32:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121845969148,77.0432331677059&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 55, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(75, 8, '', '25.351759432317184', '51.50236062705517', 'Onaiza Street, Doha, Qatar', '', '', 'Set your drop point', 'Tuesday, Nov 21', '20:54:24', '08:54:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.351759432317184,51.50236062705517&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-21'),
(76, 8, '', '25.351759432317184', '51.50236062705517', 'Onaiza Street, Doha, Qatar', '25.2912323', '51.53232860000001', 'Souq Waqif', 'Tuesday, Nov 21', '20:55:11', '09:02:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.351759432317184,51.50236062705517&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 66, 2, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-21'),
(77, 8, '', '25.351759432317184', '51.50236062705517', 'Onaiza Street, Doha, Qatar', '25.324906499999997', '51.53045029999999', 'City Center Doha Shopping Mall', 'Tuesday, Nov 21', '21:12:36', '09:18:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|25.351759432317184,51.50236062705517&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 66, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(78, 19, '', '28.4121047355746', '77.0432780453858', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4145734672855', '77.0719164982438', '78, Tigra, Sector 57\nGurugram, Haryana 122003', 'Wednesday, Nov 22', '06:46:17', '06:46:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121047355746,77.0432780453858&markers=color:red|label:D|28.4145734672855,77.0719164982438&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 55, 4, 1, 1, 7, 1, 0, 2, 0, 1, 1, '2017-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '999', 1),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '999', 1),
(6, 'Breakdown', '199', 1),
(7, 'Breakdown', '8001001', 1),
(8, 'K10', '0800187241', 1),
(9, 'BOMBEROS', '911', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppourt`
--

INSERT INTO `suppourt` (`sup_id`, `driver_id`, `name`, `email`, `phone`, `query`) VALUES
(1, 212, 'rohit', 'rohit', '8950200340', 'np'),
(2, 212, 'shilpa', 'shilpa', 'fgffchchch', 'yffhjkjhk'),
(3, 282, 'zak', 'zak', '0628926431', 'Hi the app driver is always crashing on android devices.also the gps  position is not taken by the application.regards'),
(4, 476, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SÓ CHAMAR NO WHATS ', 'SÓ CHAMAR NO WHATS '),
(5, 477, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SEJA BEM VINDO', 'SEJA BEM VINDO'),
(6, 643, 'ewqla', 'lsajlas@jdsal', 'lsalk', 'lsalk');

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Car Registration'),
(2, 'Driving License '),
(3, 'ID'),
(4, 'Insurance ');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(20, 3, 2, 1),
(19, 3, 1, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(23, 84, 8, 1),
(22, 84, 6, 1),
(21, 3, 4, 1),
(24, 124, 1, 1),
(25, 124, 2, 1),
(26, 124, 3, 1),
(27, 126, 1, 1),
(28, 126, 2, 1),
(29, 126, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_bill`
--

INSERT INTO `table_driver_bill` (`bill_id`, `bill_from_date`, `bill_to_date`, `driver_id`, `outstanding_amount`, `bill_settle_date`, `bill_status`) VALUES
(1, '2017-10-10 00.00.01 AM', '2017-10-10 02:00:22 PM', 1, '4', '2017-11-13', 1),
(2, '2017-10-10 02:01:22 PM', '2017-10-11 07:25:54 AM', 3, '0', '0000-00-00', 0),
(3, '2017-10-10 02:01:22 PM', '2017-10-11 07:25:54 AM', 2, '0.9', '0000-00-00', 0),
(4, '2017-10-10 02:01:22 PM', '2017-10-11 07:25:54 AM', 1, '0', '0000-00-00', 0),
(5, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 57, '0', '2017-11-21', 1),
(6, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 56, '0', '0000-00-00', 0),
(7, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 55, '64.08', '0000-00-00', 0),
(8, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 54, '0', '0000-00-00', 0),
(9, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 53, '0', '0000-00-00', 0),
(10, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 52, '0', '0000-00-00', 0),
(11, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 51, '0', '0000-00-00', 0),
(12, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 50, '0', '2017-11-21', 1),
(13, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 49, '0', '0000-00-00', 0),
(14, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 48, '0', '0000-00-00', 0),
(15, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 47, '0', '0000-00-00', 0),
(16, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 46, '0', '0000-00-00', 0),
(17, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 45, '0', '0000-00-00', 0),
(18, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 44, '-159.2', '0000-00-00', 0),
(19, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 43, '5', '0000-00-00', 0),
(20, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 42, '5', '0000-00-00', 0),
(21, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 41, '-34.6', '0000-00-00', 0),
(22, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 40, '0', '0000-00-00', 0),
(23, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 39, '0', '0000-00-00', 0),
(24, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 38, '4', '0000-00-00', 0),
(25, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 37, '0', '0000-00-00', 0),
(26, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 36, '0', '0000-00-00', 0),
(27, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 35, '0', '0000-00-00', 0),
(28, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 34, '0', '0000-00-00', 0),
(29, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 33, '0', '0000-00-00', 0),
(30, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 32, '0', '0000-00-00', 0),
(31, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 31, '0', '0000-00-00', 0),
(32, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 30, '0', '0000-00-00', 0),
(33, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 29, '0', '0000-00-00', 0),
(34, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 28, '0', '0000-00-00', 0),
(35, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 27, '0', '0000-00-00', 0),
(36, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 26, '0', '0000-00-00', 0),
(37, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 25, '0', '0000-00-00', 0),
(38, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 24, '0', '0000-00-00', 0),
(39, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 23, '0', '0000-00-00', 0),
(40, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 22, '0', '0000-00-00', 0),
(41, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 21, '0', '0000-00-00', 0),
(42, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 20, '0', '0000-00-00', 0),
(43, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 19, '0', '0000-00-00', 0),
(44, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 18, '0', '0000-00-00', 0),
(45, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 17, '0', '0000-00-00', 0),
(46, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 16, '0', '0000-00-00', 0),
(47, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 15, '0', '0000-00-00', 0),
(48, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 14, '0', '0000-00-00', 0),
(49, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 13, '0', '0000-00-00', 0),
(50, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 12, '0', '0000-00-00', 0),
(51, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 11, '0', '0000-00-00', 0),
(52, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 10, '0', '0000-00-00', 0),
(53, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 9, '0', '0000-00-00', 0),
(54, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 8, '0', '0000-00-00', 0),
(55, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 7, '24', '0000-00-00', 0),
(56, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 6, '4', '0000-00-00', 0),
(57, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 5, '19', '0000-00-00', 0),
(58, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 4, '30', '0000-00-00', 0),
(59, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 3, '0', '0000-00-00', 0),
(60, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 2, '0', '0000-00-00', 0),
(61, '2017-10-11 07:26:54 AM', '2017-11-21 02:02:52 PM', 1, '0', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1507631923document_image_13.jpg', '10-10-2017', 1),
(2, 1, 2, 'uploads/driver/1507631934document_image_12.jpg', '10-10-2017', 1),
(3, 1, 4, 'uploads/driver/1507631951document_image_14.jpg', '10-10-2017', 1),
(4, 2, 1, 'uploads/driver/1507641643document_image_21.jpg', '10-10-2017', 1),
(5, 2, 2, 'uploads/driver/1507641679document_image_22.jpg', '10-10-2017', 1),
(6, 2, 3, 'uploads/driver/1507641701document_image_23.jpg', '10-10-2017', 1),
(7, 3, 3, 'uploads/driver/1507644815document_image_33.jpg', '27-10-2017', 1),
(8, 3, 2, 'uploads/driver/1507644823document_image_32.jpg', '28-10-2017', 1),
(9, 3, 4, 'uploads/driver/1507644831document_image_34.jpg', '28-10-2017', 1),
(10, 4, 3, 'uploads/driver/1507802999document_image_43.jpg', '2017-10-28', 1),
(11, 4, 2, 'uploads/driver/1507803006document_image_42.jpg', '2017-10-28', 1),
(12, 4, 4, 'uploads/driver/1507803013document_image_44.jpg', '2017-10-28', 1),
(13, 5, 3, 'uploads/driver/1507817635document_image_53.jpg', '12-10-2017', 1),
(14, 5, 2, 'uploads/driver/1507817642document_image_52.jpg', '12-10-2017', 1),
(15, 5, 4, 'uploads/driver/1507817651document_image_54.jpg', '12-10-2017', 1),
(16, 6, 3, 'uploads/driver/1508742471document_image_63.jpg', '23-10-2017', 1),
(17, 6, 2, 'uploads/driver/1508742478document_image_62.jpg', '23-10-2017', 1),
(18, 6, 4, 'uploads/driver/1508742485document_image_64.jpg', '23-10-2017', 1),
(19, 7, 1, 'uploads/driver/1508769581document_image_71.jpg', '23-10-2017', 1),
(20, 7, 2, 'uploads/driver/1508769601document_image_72.jpg', '23-10-2017', 1),
(21, 7, 3, 'uploads/driver/1508769621document_image_73.jpg', '23-10-2017', 1),
(22, 8, 3, 'uploads/driver/1508823816document_image_83.jpg', '28-10-2017', 1),
(23, 8, 2, 'uploads/driver/1508823826document_image_82.jpg', '28-10-2017', 1),
(24, 8, 4, 'uploads/driver/1508823836document_image_84.jpg', '28-10-2017', 1),
(25, 25, 2, 'uploads/driver/1510570386document_image_252.jpg', '13-11-2017', 1),
(26, 25, 3, 'uploads/driver/1510570405document_image_253.jpg', '13-11-2017', 1),
(27, 25, 4, 'uploads/driver/1510570418document_image_254.jpg', '13-11-2017', 1),
(28, 26, 2, 'uploads/driver/1510570431document_image_262.jpg', '2017-11-30', 1),
(29, 26, 3, 'uploads/driver/1510570524document_image_263.jpg', '2017-11-30', 1),
(30, 26, 4, 'uploads/driver/1510570532document_image_264.jpg', '2017-11-30', 1),
(31, 30, 2, 'uploads/driver/1510572420document_image_302.jpg', '13-11-2017', 1),
(32, 30, 4, 'uploads/driver/1510572477document_image_304.jpg', '13-11-2017', 1),
(33, 30, 3, 'uploads/driver/1510572489document_image_303.jpg', '13-11-2017', 1),
(34, 31, 3, 'uploads/driver/1510573371document_image_313.jpg', '13-11-2017', 1),
(35, 34, 2, 'uploads/driver/1510573765document_image_342.jpg', '13-11-2017', 1),
(36, 34, 3, 'uploads/driver/1510573773document_image_343.jpg', '13-11-2017', 1),
(37, 34, 4, 'uploads/driver/1510573783document_image_344.jpg', '13-11-2017', 1),
(38, 36, 2, 'uploads/driver/1510574619document_image_362.jpg', '13-11-2017', 1),
(39, 36, 3, 'uploads/driver/1510574627document_image_363.jpg', '13-11-2017', 1),
(40, 36, 4, 'uploads/driver/1510574635document_image_364.jpg', '17-11-2017', 1),
(41, 38, 2, 'uploads/driver/1510578290document_image_382.jpg', '2017-11-30', 1),
(42, 38, 3, 'uploads/driver/1510578298document_image_383.jpg', '2017-11-30', 1),
(43, 38, 4, 'uploads/driver/1510578305document_image_384.jpg', '2017-11-30', 1),
(44, 40, 2, 'uploads/driver/1510594555document_image_402.jpg', '13-11-2017', 1),
(45, 40, 3, 'uploads/driver/1510594564document_image_403.jpg', '13-11-2017', 1),
(46, 40, 4, 'uploads/driver/1510594620document_image_404.jpg', '13-11-2017', 1),
(47, 41, 2, 'uploads/driver/1510665847document_image_412.jpg', '14-11-2017', 1),
(48, 41, 3, 'uploads/driver/1510665857document_image_413.jpg', '14-11-2017', 1),
(49, 41, 4, 'uploads/driver/1510665867document_image_414.jpg', '14-11-2017', 1),
(50, 42, 2, 'uploads/driver/1510737750document_image_422.jpg', '2017-11-30', 1),
(51, 42, 3, 'uploads/driver/1510737777document_image_423.jpg', '2017-11-30', 1),
(52, 42, 4, 'uploads/driver/1510737847document_image_424.jpg', '2017-11-30', 1),
(53, 43, 2, 'uploads/driver/1510739890document_image_432.jpg', '2017-11-30', 1),
(54, 43, 3, 'uploads/driver/1510739911document_image_433.jpg', '2017-11-30', 1),
(55, 43, 4, 'uploads/driver/1510739924document_image_434.jpg', '2017-11-30', 1),
(56, 44, 2, 'uploads/driver/1510857298document_image_442.jpg', '17-11-2017', 1),
(57, 44, 3, 'uploads/driver/1510857305document_image_443.jpg', '17-11-2017', 1),
(58, 44, 4, 'uploads/driver/1510857312document_image_444.jpg', '17-11-2017', 1),
(59, 46, 1, 'uploads/driver/1510997293document_image_461.jpg', '18-11-2017', 2),
(60, 46, 2, 'uploads/driver/1510998608document_image_462.jpg', '18-11-2017', 2),
(61, 46, 3, 'uploads/driver/1510998623document_image_463.jpg', '18-11-2017', 2),
(62, 47, 2, 'uploads/driver/1510998963document_image_472.jpg', '18-11-2017', 1),
(63, 47, 3, 'uploads/driver/1510998973document_image_473.jpg', '18-11-2017', 1),
(64, 47, 4, 'uploads/driver/1510998984document_image_474.jpg', '18-11-2017', 1),
(65, 48, 2, 'uploads/driver/1510999983document_image_482.jpg', '18-11-2017', 1),
(66, 48, 4, 'uploads/driver/1511000002document_image_484.jpg', '18-11-2017', 1),
(67, 48, 3, 'uploads/driver/1511000034document_image_483.jpg', '18-11-2017', 1),
(68, 49, 2, 'uploads/driver/1511001663document_image_492.jpg', '2017-11-30', 1),
(69, 49, 3, 'uploads/driver/1511001669document_image_493.jpg', '2017-11-30', 1),
(70, 49, 4, 'uploads/driver/1511001680document_image_494.jpg', '2017-11-30', 1),
(71, 55, 2, 'uploads/driver/1511246657document_image_552.jpg', '2017-11-30', 1),
(72, 55, 3, 'uploads/driver/1511246666document_image_553.jpg', '2017-11-30', 1),
(73, 55, 4, 'uploads/driver/1511246674document_image_554.jpg', '2017-11-30', 1),
(74, 56, 1, 'uploads/driver/1511271560document_image_561.jpg', '30-11-2017', 1),
(75, 56, 2, 'uploads/driver/1511271580document_image_562.jpg', '30-11-2017', 1),
(76, 56, 3, 'uploads/driver/1511271604document_image_563.jpg', '30-11-2017', 1),
(77, 57, 1, 'uploads/driver/1511272738document_image_571.jpg', '30-11-2017', 1),
(78, 57, 2, 'uploads/driver/1511272783document_image_572.jpg', '30-11-2017', 1),
(79, 57, 3, 'uploads/driver/1511272804document_image_573.jpg', '30-11-2017', 1),
(80, 58, 4, 'uploads/driver/15112731140cars.pngdocument_image_58.png', '2017-12-01', 2),
(81, 58, 2, 'uploads/driver/15112731141cars.pngdocument_image_58.png', '2017-11-30', 2),
(82, 58, 3, 'uploads/driver/15112731142cars.pngdocument_image_58.png', '2017-11-23', 2),
(83, 59, 2, 'uploads/driver/1511277187document_image_592.jpg', '21-11-2017', 1),
(84, 59, 3, 'uploads/driver/1511277196document_image_593.jpg', '21-11-2017', 1),
(85, 59, 4, 'uploads/driver/1511277203document_image_594.jpg', '21-11-2017', 1),
(86, 65, 1, 'uploads/driver/1511284841document_image_651.jpg', '30-11-2017', 1),
(87, 65, 2, 'uploads/driver/1511284866document_image_652.jpg', '30-11-2017', 1),
(88, 65, 3, 'uploads/driver/1511284886document_image_653.jpg', '30-11-2017', 1),
(89, 66, 1, 'uploads/driver/1511297001document_image_661.jpg', '30-11-2017', 1),
(90, 66, 2, 'uploads/driver/1511297019document_image_662.jpg', '30-11-2017', 1),
(91, 66, 3, 'uploads/driver/1511297034document_image_663.jpg', '30-11-2017', 1),
(92, 69, 2, 'uploads/driver/1511319499document_image_692.jpg', '22-11-2017', 1),
(93, 69, 3, 'uploads/driver/1511319510document_image_693.jpg', '22-11-2017', 1),
(94, 69, 4, 'uploads/driver/1511319523document_image_694.jpg', '22-11-2017', 1),
(95, 77, 2, 'uploads/driver/1511507598document_image_772.jpg', '24-11-2017', 1),
(96, 77, 3, 'uploads/driver/1511507642document_image_773.jpg', '24-11-2017', 1),
(97, 81, 3, 'uploads/driver/1511516638document_image_813.jpg', '24-11-2017', 1),
(98, 93, 2, 'uploads/driver/1511525757document_image_932.jpg', '24-11-2017', 1),
(99, 93, 3, 'uploads/driver/1511525765document_image_933.jpg', '24-11-2017', 1),
(100, 93, 4, 'uploads/driver/1511525779document_image_934.jpg', '24-11-2017', 1),
(101, 100, 2, 'uploads/driver/1511767320document_image_1002.jpg', '2017-11-30', 1),
(102, 100, 3, 'uploads/driver/1511767327document_image_1003.jpg', '2017-11-30', 1),
(103, 100, 4, 'uploads/driver/1511767334document_image_1004.jpg', '2017-11-30', 1),
(104, 99, 2, 'uploads/driver/1511768420document_image_992.jpg', '27-11-2017', 1),
(105, 99, 3, 'uploads/driver/1511768430document_image_993.jpg', '27-11-2017', 1),
(106, 99, 4, 'uploads/driver/1511768439document_image_994.jpg', '27-11-2017', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-10-10 11:39:24', '', '', 0, 0, '2017-10-10'),
(2, 2, '2017-10-10 20:08:04', '2017-10-11 06:01:35', '9 Hours 156 Minutes', 9, 156, '2017-10-10'),
(3, 3, '2017-10-10 15:14:08', '2017-10-10 16:03:38', '0 Hours 49 Minutes', 0, 49, '2017-10-10'),
(4, 2, '2017-10-11 06:29:19', '2017-10-11 06:06:28', '0 Hours 3 Minutes', 0, 3, '2017-10-11'),
(5, 4, '2017-10-12 11:10:22', '2017-10-17 11:17:12', '0 Hours 6 Minutes', 0, 6, '2017-10-12'),
(6, 5, '2017-10-12 15:14:28', '', '', 0, 0, '2017-10-12'),
(7, 4, '2017-10-17 11:42:34', '', '', 0, 0, '2017-10-17'),
(8, 4, '2017-10-23 06:49:29', '2017-11-08 08:54:53', '4 Hours 6 Minutes', 4, 6, '2017-10-23'),
(9, 6, '2017-10-23 08:08:16', '', '', 0, 0, '2017-10-23'),
(10, 7, '2017-10-23 19:52:04', '2017-11-21 16:45:38', '24 Hours 62 Minutes', 24, 62, '2017-10-23'),
(11, 25, '2017-11-13 10:53:50', '', '', 0, 0, '2017-11-13'),
(12, 26, '2017-11-13 10:55:44', '2017-11-13 11:35:02', '0 Hours 39 Minutes', 0, 39, '2017-11-13'),
(13, 30, '2017-11-13 11:28:18', '2017-11-13 11:42:02', '0 Hours 13 Minutes', 0, 13, '2017-11-13'),
(14, 34, '2017-11-13 11:49:50', '2017-11-13 11:50:31', '0 Hours 0 Minutes', 0, 0, '2017-11-13'),
(15, 36, '2017-11-13 12:04:28', '2017-11-13 13:20:29', '1 Hours 16 Minutes', 1, 16, '2017-11-13'),
(16, 38, '2017-11-13 13:05:14', '2017-11-13 13:21:51', '0 Hours 16 Minutes', 0, 16, '2017-11-13'),
(17, 40, '2017-11-13 17:37:08', '2017-11-14 13:35:35', '19 Hours 58 Minutes', 19, 58, '2017-11-13'),
(18, 41, '2017-11-14 13:26:01', '', '', 0, 0, '2017-11-14'),
(19, 40, '2017-11-14 18:26:42', '', '', 0, 0, '2017-11-14'),
(20, 42, '2017-11-15 09:24:34', '2017-11-15 09:55:00', '0 Hours 30 Minutes', 0, 30, '2017-11-15'),
(21, 43, '2017-11-15 09:59:03', '', '', 0, 0, '2017-11-15'),
(22, 44, '2017-11-16 18:35:23', '', '', 0, 0, '2017-11-16'),
(23, 46, '2017-11-18 09:51:10', '', '', 0, 0, '2017-11-18'),
(24, 47, '2017-11-18 09:56:34', '', '', 0, 0, '2017-11-18'),
(25, 49, '2017-11-18 10:41:27', '2017-11-22 04:45:02', '38 Hours 17 Minutes', 38, 17, '2017-11-18'),
(26, 55, '2017-11-21 06:44:42', '', '', 0, 0, '2017-11-21'),
(27, 59, '2017-11-21 15:13:32', '', '', 0, 0, '2017-11-21'),
(28, 66, '2017-11-21 20:44:12', '', '', 0, 0, '2017-11-21'),
(29, 93, '2017-11-24 12:16:27', '', '', 0, 0, '2017-11-24'),
(30, 100, '2017-11-27 07:22:21', '', '', 0, 0, '2017-11-27'),
(31, 56, '2017-11-28 22:04:14', '', '', 0, 0, '2017-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(42, 'Portuguese', 1),
(41, 'Vietnamese', 2),
(40, 'French', 2),
(39, 'Spanish', 1),
(38, 'Arabic', 1),
(37, 'Arabic', 1),
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Succeffully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 3, 1, 0, '', 1, 4, '', '2017-10-10'),
(2, 1, 1, 4, '', 1, 0, '', '2017-10-10'),
(3, 7, 2, 0, '', 2, 1, 'mÃ¡s o menos', '2017-10-11'),
(4, 5, 2, 5, 'muy buen servicio', 2, 0, '', '2017-10-11'),
(5, 12, 4, 5, '', 4, 4, '', '2017-10-12'),
(6, 13, 4, 0, '', 4, 5, 'Hshsjshsb hajabs', '2017-10-12'),
(7, 15, 5, 0, '', 5, 4.5, '', '2017-10-12'),
(8, 16, 5, 4, '', 5, 5, '', '2017-10-12'),
(9, 9, 5, 4, '', 5, 0, '', '2017-10-12'),
(10, 18, 5, 0, '', 5, 4, '', '2017-10-12'),
(11, 20, 4, 0, '', 4, 5, '', '2017-10-13'),
(12, 25, 4, 0, '', 4, 5, 'Jfjxdh', '2017-10-23'),
(13, 26, 4, 4.5, 'Ufjcjvjcjvjcjxhxh hcjvjvjvjvjgjvj', 4, 5, 'Gdhdhfhfjfjfjfi bshshsbsbshsshbdbdbdbsjshdbdhhdbddbbdbdhdbdbd hshshshbshdhxbdbdjdbdjdjdbdndjbdbdjdjdjdbdjxhdbdj polio bshshhsbs hshshjsbsjddh', '2017-10-23'),
(14, 27, 4, 4.5, '', 4, 5, 'Vshshsbsbsbshs', '2017-10-23'),
(15, 28, 6, 4.5, '', 6, 4.5, '', '2017-10-23'),
(16, 17, 7, 5, '', 7, 0, '', '2017-10-23'),
(17, 19, 8, 0, '', 7, 0, '', '2017-10-23'),
(18, 37, 9, 0, '', 38, 5, '', '2017-11-13'),
(19, 38, 12, 0, '', 41, 5, '', '2017-11-14'),
(20, 22, 12, 5, '', 41, 0, '', '2017-11-14'),
(21, 45, 12, 0, '', 41, 5, '', '2017-11-14'),
(22, 23, 12, 5, '', 41, 0, '', '2017-11-14'),
(23, 47, 13, 0, '', 42, 5, '', '2017-11-15'),
(24, 50, 14, 0, '', 43, 5, '', '2017-11-15'),
(25, 56, 14, 0, '', 43, 0, '', '2017-11-15'),
(26, 57, 5, 0, '', 44, 5, '', '2017-11-16'),
(27, 58, 5, 0, '', 44, 4, '', '2017-11-17'),
(28, 59, 5, 0, '', 44, 5, '', '2017-11-17'),
(29, 30, 5, 5, 'v', 44, 0, '', '2017-11-17'),
(30, 60, 5, 0, '', 44, 4.5, '', '2017-11-17'),
(31, 31, 5, 4.5, '', 44, 0, '', '2017-11-17'),
(32, 61, 5, 0, '', 44, 4.5, '', '2017-11-17'),
(33, 32, 5, 4.5, '', 44, 0, '', '2017-11-17'),
(34, 62, 5, 0, '', 44, 4.5, '', '2017-11-17'),
(35, 33, 5, 4, '', 44, 0, '', '2017-11-17'),
(36, 29, 5, 4.5, '', 44, 0, '', '2017-11-17'),
(37, 63, 5, 0, '', 44, 4, '', '2017-11-17'),
(38, 34, 5, 5, '', 44, 0, '', '2017-11-17'),
(39, 64, 5, 0, '', 44, 5, '', '2017-11-17'),
(40, 35, 5, 4.5, '', 44, 0, '', '2017-11-17'),
(41, 65, 5, 0, '', 44, 4.5, '', '2017-11-17'),
(42, 36, 5, 4.5, '', 44, 0, '', '2017-11-17'),
(43, 66, 13, 4.5, '', 49, 5, '', '2017-11-18'),
(44, 67, 19, 0, '', 55, 5, '', '2017-11-21'),
(45, 68, 19, 0, '', 55, 5, '', '2017-11-21'),
(46, 69, 19, 5, '', 55, 5, '', '2017-11-21'),
(47, 71, 19, 0, '', 55, 5, '', '2017-11-21'),
(48, 73, 19, 5, '', 55, 5, '', '2017-11-21'),
(49, 74, 19, 0, '', 55, 5, '', '2017-11-21'),
(50, 76, 8, 0, '', 66, 5, 'ok', '2017-11-21'),
(51, 44, 8, 0, '', 66, 0, '', '2017-11-21'),
(52, 77, 8, 0, '', 66, 3, 'kh', '2017-11-21'),
(53, 78, 19, 0, '', 55, 5, '', '2017-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 1, 0, 1),
(2, 1, 1, 0, 2),
(3, 1, 1, 1, 3),
(4, 1, 1, 1, 4),
(5, 1, 1, 1, 5),
(6, 1, 2, 2, 6),
(7, 1, 2, 2, 7),
(8, 1, 2, 0, 8),
(9, 1, 2, 0, 9),
(10, 1, 6, 4, 10),
(11, 1, 6, 4, 11),
(12, 1, 4, 4, 12),
(13, 1, 4, 4, 13),
(14, 1, 5, 0, 14),
(15, 1, 5, 5, 15),
(16, 1, 5, 5, 16),
(17, 1, 5, 5, 17),
(18, 1, 5, 5, 18),
(19, 1, 4, 0, 19),
(20, 1, 4, 4, 20),
(21, 1, 4, 0, 21),
(22, 1, 4, 0, 22),
(23, 1, 4, 4, 25),
(24, 1, 4, 4, 26),
(25, 1, 4, 4, 27),
(26, 1, 6, 6, 28),
(27, 1, 7, 7, 29),
(28, 1, 8, 7, 30),
(29, 1, 8, 7, 31),
(30, 1, 8, 0, 32),
(31, 1, 8, 7, 33),
(32, 1, 8, 0, 34),
(33, 1, 8, 0, 35),
(34, 1, 8, 0, 36),
(35, 1, 9, 38, 37),
(36, 1, 12, 41, 38),
(37, 1, 12, 41, 39),
(38, 1, 12, 0, 40),
(39, 1, 12, 0, 41),
(40, 1, 12, 0, 42),
(41, 1, 12, 0, 43),
(42, 1, 12, 41, 44),
(43, 1, 12, 41, 45),
(44, 1, 13, 42, 46),
(45, 1, 13, 42, 47),
(46, 1, 14, 0, 48),
(47, 1, 14, 0, 49),
(48, 1, 14, 43, 50),
(49, 1, 14, 0, 51),
(50, 1, 14, 0, 52),
(51, 1, 14, 0, 53),
(52, 1, 14, 0, 54),
(53, 1, 14, 0, 55),
(54, 1, 14, 43, 56),
(55, 1, 5, 44, 57),
(56, 1, 5, 44, 58),
(57, 1, 5, 44, 59),
(58, 1, 5, 44, 60),
(59, 1, 5, 44, 61),
(60, 1, 5, 44, 62),
(61, 1, 5, 44, 63),
(62, 1, 5, 44, 64),
(63, 1, 5, 44, 65),
(64, 1, 13, 49, 66),
(65, 1, 19, 55, 67),
(66, 1, 19, 55, 68),
(67, 1, 19, 55, 69),
(68, 1, 19, 0, 70),
(69, 1, 19, 55, 71),
(70, 1, 19, 0, 72),
(71, 1, 19, 55, 73),
(72, 1, 19, 55, 74),
(73, 1, 8, 0, 75),
(74, 1, 8, 66, 76),
(75, 1, 8, 66, 77),
(76, 1, 19, 55, 78);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(5, 1, 'Apporio devices .', '', '+918080808080', '', '', '', 0, '800', 'Thursday, Oct 12', '', 0, 0, 0, 0, 0, '', '', '', '', '', '111976537605501585996', 'Apporio devices', 'apporiodevices@gmail.com', 'null', '', '', 0, 0, '2.725', 0, '', 3, '2017-10-12', 1),
(3, 1, '3 40', '@dj', 'd;aldlk', 'd', '', '', 0, '0', 'Thursday, Oct 12', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-12', 1),
(4, 1, 'user .', 'u@7.com', '+911234561234', 'qwerty', '', '', 0, '0', 'Thursday, Oct 12', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.83333333333', 0, '', 1, '2017-10-12', 1),
(6, 1, 'sgvssv .', 'svvssv@gmail.com', '+91696969696969', '123456', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.5', 0, '', 1, '2017-10-23', 1),
(7, 1, 'anna .', 'anna@noqoody.com', '+97433363001', '123q', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-23', 1),
(8, 1, 'nayef', 'md@qmobile.me', '+97455320001', '12345678', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2', 0, '', 1, '2017-10-23', 1),
(9, 1, 'vababa .', 'hahab@89.com', '+917410852963', 'qwerty', '', '', 0, '0', 'Monday, Nov 13', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-11-13', 1),
(10, 1, 'Qmobile MobileApps', '', '+97433300020', '', '', '', 0, '50', 'Tuesday, Nov 14', '', 0, 0, 0, 0, 0, '339117106553154', 'saneeb@qmobileme.com', 'http://graph.facebook.com/339117106553154/picture?type=large', 'Qmobile', 'MobileApps', '', '', '', '', '', '', 0, 0, '', 0, '', 2, '2017-11-14', 1),
(11, 1, 'Khaldoun Baz .', '', '+97455642929', '', '', '', 0, '0', 'Tuesday, Nov 14', '', 0, 0, 0, 0, 0, '10159521382445103', 'khillo_baz@hotmail.com', 'http://graph.facebook.com/10159521382445103/picture?type=large', 'Khaldoun', 'Baz', '113191431630003443417', 'Khaldoun Baz', 'k.baz@qmobileme.com', 'null', '', '', 0, 0, '', 0, '', 2, '2017-11-14', 1),
(12, 1, 'anurag .', 'anurag@apporio.com', '+918874531856', 'qwerty', 'http://apporio.org.com/Ojraa/ojraa/uploads/1510665997588.jpg', '', 0, '200', 'Tuesday, Nov 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '3.33333333333', 0, '', 1, '2017-11-14', 1),
(13, 1, 'hfhffhhfgffh .', 'jvjgj@67.com', '+916666666666', 'qwerty', '', '', 0, '0', 'Wednesday, Nov 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-11-15', 1),
(14, 1, 'fofj .', 'kfk@46.com', '+910000011111', 'qwerty', '', '', 0, '0', 'Wednesday, Nov 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-11-15', 1),
(15, 1, 'hhxhx .', 'svvzxhhxhdh@g.com', '+9745655658656', '123456', '', '', 0, '0', 'Friday, Nov 17', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-17', 1),
(16, 1, 'Manish Sharma .', '', '+9749090909090', '', '', '', 0, '0', 'Saturday, Nov 18', '', 0, 0, 0, 0, 0, '', '', '', '', '', '114124603815519089917', 'Manish Sharma', 'itsme.095manish@gmail.com', 'https://lh5.googleusercontent.com/-IZ31YJ4pPvk/AAAAAAAAAAI/AAAAAAAAJrM/8_z3nJ3Cy28/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-11-18', 1),
(17, 1, 'bana .', 'gah@90.com', '+911234567234', 'qwerty', '', '', 0, '400', 'Saturday, Nov 18', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-18', 1),
(18, 1, 'Mandy .', 'mandy@yu.com', '+914545454545', 'qwerty', '', '', 0, '500', 'Monday, Nov 20', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-20', 1),
(19, 1, 'tfds .', 'kyu@90.com', '+914444455555', 'qwerty', 'http://apporio.org.com/Ojraa/ojraa/uploads/swift_file69.jpeg', '', 0, '0', 'Tuesday, Nov 21', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '5', 0, '', 1, '2017-11-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, 'dhV1hvONTt4:APA91bHg_DP5UGqlfQg0U3isdjuonClI-NfzXvWiB5IQfpI7VVhGn8i70yEtBSi4TlIsyoh1WR6nI1g04IrdRJ6Qtr265n7DGZ3hzKKOSRTsEA5UtGypCy0i3UTNPSsFDsA_jp6JZ1dY', 0, 'c06c40037632ed39', 0),
(2, 1, 'cB1UxPXe9i0:APA91bG5UbYZSEIh9O93H9HD2Ts3s_YLVW5w4RHPiVh4fnXueZ7b0w9XWGP9cl9NwyzGDOO_TZTmm8hlmFPTdVZlbZaL5OaYVjAWqzkKlnZn8r3bKPAa8RzkDI296QQJULQy9HBpj-Wn', 0, 'eff54ff52339245c', 1),
(3, 2, 'fYXgmXi5btI:APA91bGRSevFxb2Fre2adgJKb_ErrzO8UROArk1bOj48DinOgIkqg1V1bhFE-lbuGOoD5dOobeVicHqhc5IxBEk2LBbBt_H7wxz_97Fs3s7LGMluHVjXlUv2gFJS_Nz3Yj2DI7KTTRiW', 0, '4f4107385f41fccf', 0),
(4, 4, '86CC8B657CC22EE2E8F1A8C4869359DDDB0124E1AFD23228D488367E16C1BC85', 1, '6BCE425B-5B0A-4BBD-8070-BAEFBD888B69', 0),
(5, 6, 'eguf3qC4VrE:APA91bHLeQ7RRn2XS5BQyYFC_tB_aL20Px50Y1SyDbIFTxg-LhXQbCoZdfdg0i2Zt6-jtG9o4daYKFBEVo86RlqNp9JsqlCVDXTrwCY4337TyhB9XxVa3li5KPfcOBvs0_n4KsQXjQ2x', 0, 'a449f5c72f1dbcf8', 1),
(6, 4, 'D59485B2D5572CC561FFF1EDBC60774A4FDE51436CEF3D401F32F8B0D65CAC22', 1, '62775257-BD71-4E56-9362-6CBB82C7C3FF', 1),
(7, 4, '47CB9713475297392B5662DF4EC29EBE455672A1F696FDA17C19A96118BA1554', 1, '003C4C8B-31EA-4367-B328-70117DA16A46', 1),
(8, 7, 'dEnOejAVR3E:APA91bGPigfhgoPlaTU6YapuL0Cas2TB4p35TmMGWBlw5bPGALWG9pd_YoxsG6AwJYon75DOgIhr0iLCsjwFaYU_XaiEbOWyUvxF7aHHWv-Eg6dylc58yb-lu4ZzcduWTkei1uYDirTO', 0, '371d4ab2bebc7518', 1),
(9, 8, 'fh1SQZNbJng:APA91bGKi_ORMnkOjhUPx0ChJnzskQpFjKK8R3kbtu-wHzL04rV_uEj8mpITlc5KE2O2zscjfpbbIFeE_bzc4W3UNa5kcotWqMDox1J0uY9QT5o4HVniRa2ymefs5kodV16n0DxCIELY', 0, '5a341dbb31e6ad55', 1),
(10, 14, 'AE5E6ABC54A43F9A7DD81391D85410A04AD83051066AF1C3DDF54E4A0714416C', 1, '6C29C534-358D-4B78-9EE3-754DB97EDC92', 1),
(11, 10, 'cuxklAcdkYs:APA91bE7HTR1EMuuiyuMSgdQBrPCkWJqjG-9P3VI0TUHKzz-iKtWD7EkS-bYQbcTLMjoZvfm6Bmec-8gXNOEEQSr1wFg_ypIf_A5gbCW313RlXgR0CKdq2f6hnSxwzU5ylWuu9bR608r', 0, '55c6dc069472540d', 1),
(12, 11, 'faz3aVqtlvQ:APA91bGo_z5uFpMarevCW_8TWmZXSlrnY-_jzRq3ybgxGEBYze8zBlLnuioFUGZBWh3HL42yxHjjme8iZVTT4DhqJKq1pZ9QzpKo1FQhLTCbBiL2PUT9pyM4DDtJwUsKCV20oo0sBmT7', 0, 'e4208a3c6074836a', 1),
(13, 12, 'cjHTkipt04w:APA91bGRgfU9d7M67gIVJP43AtbyJBjp2b1KhC2V7-gv0ET720kp7iPdGE80vizP8EgTH-85po9iNx_XZL8t6__-rcDezoVailKXNlJTbPiELEprapr9jhvbWaSwihTXyWFepKTFh30k', 0, 'a4594951bbeaaafa', 1),
(14, 13, 'F66F4B7B52B9F26D2CA21EE122FBD801E824695DFDB361852E2B0CC04DA85030', 1, '33622763-D3BB-483A-8CC5-36CC325FB087', 1),
(15, 5, 'c2gJtLHa2CI:APA91bFNXEi0Mowu-cmWupchyzaAlIJSSwVu2mdqITC5-DWjHDiV62Bs-8szj1OFzdMfH6VDeOQBiOWjt1YqMT6txgaaNuPxf0W9GXmlChHkUrylxrS-XWq_PIdX8_5nLJBio0t_rlEk', 0, '2ba9bf317a129dea', 1),
(16, 17, '5F5B07068C470E8A14F9C5760FB43AD454537ABF8B167813705EA10EAADF5A2B', 1, 'F6B80840-1A3D-4EF7-AD63-7928798A0946', 1),
(17, 18, '00025FD7A0A7DC521BC7CDFD3E0BE8F5425C960EF7ABCFD6E7DA45F5A6E30E88', 1, 'DE3A6B21-D293-4C48-89A6-8731CD7FD90C', 1),
(18, 11, 'f-eLZ-ycxaA:APA91bHHGw02zvBLz2uPOPi-zQ06jgkNdlYajkOV05Am6Zw0iMARotaRJ8v05sR70tSqVd8XrzNyDOXMQNobePc7gsjCx2gLS9gDk-U5rYbrduXNnO6l3uNZM3uzf5fcZeOp5i3QVlmI', 0, '88e5435e7dc10d3d', 1),
(19, 19, '0FCED1498FEFF7869611CEAF9ED3C26E09E457F54A23474CEA843B6F2378F180', 1, '1B4A67EC-80CA-4F41-AF58-2CE8465DF3C9', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_home_pages`
--

CREATE TABLE `web_home_pages` (
  `page_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home_pages`
--

INSERT INTO `web_home_pages` (`page_id`, `heading`, `content`, `image`, `link`) VALUES
(1, 'Cabs for Every Pocket', 'From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket', 'webstatic/img/ola-article/why-ola-1.jpg', ''),
(2, 'Secure and Safer Rides', 'Verified drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience.', 'webstatic/img/ola-article/why-ola-3.jpg', ''),
(3, 'Apporio Select', 'A membership program with Ola that lets you ride a Prime Sedan at Mini fares, book cabs without peak pricing and has zero wait time', 'webstatic/img/ola-article/why-ola-2.jpg', ''),
(4, '\r\nIn Cab Entertainment', 'Play music, watch videos and a lot more with Ola Play! Also stay connected even if you are travelling through poor network areas with our free wifi facility.', 'webstatic/img/ola-article/why-ola-9.jpg', ''),
(5, 'Cashless Rides', 'Now go cashless and travel easy. Simply recharge your Apporio money or add your credit/debit card to enjoy hassle free payments.', 'webstatic/img/ola-article/why-ola-5.jpg', ''),
(6, 'Share and Express', 'To travel with the lowest fares choose Apporio Share. For a faster travel experience we have Share Express on some fixed routes with zero deviations. Choose your ride and zoom away!', 'webstatic/img/ola-article/why-ola-3.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_currency`
--
ALTER TABLE `application_currency`
  ADD PRIMARY KEY (`application_currency_id`);

--
-- Indexes for table `application_version`
--
ALTER TABLE `application_version`
  ADD PRIMARY KEY (`application_version_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `application_currency`
--
ALTER TABLE `application_currency`
  MODIFY `application_currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_version`
--
ALTER TABLE `application_version`
  MODIFY `application_version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
