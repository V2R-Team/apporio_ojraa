<?PHP
function sendMessage(){
    $content = array(
        "en" => 'New Booking For Ride Later',
    );
    $fields = array(
        'app_id' => "0de94aab-987d-4261-b487-06dfd6e9fe17",
        'included_segments' => array('All'),
        'data' => array("template_id" =>"a32f55c8-9156-4d53-aa0e-07cc17107c2d"),
        'url'=>"http://www.apporiotaxi.com/admin/home.php?pages=ride-now",
        'contents' => $content
    );
    $fields = json_encode($fields);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
        'Authorization: Basic NDU5MmRkNjEtYmIwMC00MDAyLTg5NjctZTkxZTRmZTYzM2Ji'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}
?>