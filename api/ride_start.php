<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';
include 'location_fromlatlog.php';
$ride_id = $_REQUEST['ride_id'];
$driver_id = $_REQUEST['driver_id'];
$begin_lat = $_REQUEST['begin_lat'];
$begin_long = $_REQUEST['begin_long'];
$driver_token = $_REQUEST['driver_token'];
//$language_id = $_REQUEST['language_id'];
$language_id=1;
$log  = "ride start to road  Api - : ".date("F j, Y, g:i a").PHP_EOL.
          "ride_id :".$ride_id.PHP_EOL.
           "driver_id: ".$driver_id.PHP_EOL.
           "begin_lat: ".$begin_lat.PHP_EOL.
            "begin_long: ".$begin_long.PHP_EOL.
            "driver_token: ".$driver_token.PHP_EOL.
            "-------------------------".PHP_EOL;
file_put_contents('../logfile/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
if($ride_id!="" && $driver_id!="" && $begin_lat!="" && $begin_long!="" && $driver_token!= "")
{
    $last_time_stamp = date("h:i:s A");
    $begin_time = date("h:i:s A");
	 $query="select * from driver where driver_token='$driver_token'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
	if($ex_rows==1)
	{
	    $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date=$day.", ".$data ;
        $new_time=date("h:i");


        $query4 = "select * from ride_table where ride_id='$ride_id'";
        $result4 = $db->query($query4);
        $list4=$result4->row;
        $user_id=$list4['user_id'];
        $ride_status = $list4['ride_status'];
        $pem_file = $list4['pem_file'];
        $time_stamp = $list4['last_time_stamp'];
        $time1 = new DateTime($time_stamp);
        $time2 = new DateTime($last_time_stamp);
        $interval = $time1->diff($time2);
        $waiting_time = $interval->format('%i');

	
        $query1="UPDATE ride_table SET driver_id='$driver_id' ,last_time_stamp='$last_time_stamp', ride_status='6' WHERE ride_id='$ride_id'" ;
        $db->query($query1);

        $query4 = "select * from ride_table where ride_id='$ride_id'";
        $result4 = $db->query($query4);
        $list4=$result4->row;
        $user_id=$list4['user_id'];
        $ride_status = $list4['ride_status'];
        $begin_location = getAddress($begin_lat,$begin_long);
        $begin_location = $begin_location?$begin_location:'Address Not found';
	$query2="UPDATE done_ride SET begin_lat='$begin_lat',begin_long='$begin_long', begin_location='$begin_location',begin_time='$begin_time',waiting_time='$waiting_time' WHERE ride_id='$ride_id'";
    $db->query($query2);
			
	$query3="select * from done_ride where ride_id='$ride_id'";
	$result3 = $db->query($query3);
	$list=$result3->row;



    $query5="select * from user_device where user_id='$user_id' AND login_logout=1";
    $result5 = $db->query($query5);
    $list5=$result5->rows;

    $language="select * from messages where language_id='$language_id' and message_id=31";
	$lang_result = $db->query($language);
    $lang_list=$lang_result->row;
    $message=$lang_list['message_name'];
    $ride_id= (String) $ride_id;
    $ride_status= (String) $ride_status;

    if (!empty($list5))
     {
            foreach ($list5 as $user)
            {
                $device_id = $user['device_id'];
                $flag = $user['flag'];
                if($flag == 1)
                {
                    IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                }
                else
                {
                    AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                }
            }
        }else{
                $query5="select * from user where user_id='$user_id'";
                $result5 = $db->query($query5);
                $list5=$result5->row;
                $device_id=$list5['device_id'];

                if($device_id!="")
                {
                       if($list5['flag'] == 1)
                           {
                             IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                       } 
                           else 
                           {  
                        AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                       } 
                    }   
        }
             $language="select * from messages where language_id='$language_id' and message_id=31";
	         $lang_result = $db->query($language);
             $lang_list=$lang_result->row;
             $message=$lang_list['message_name'];
             $re = array('result'=> 1,'msg'=> $message,'details'	=> $list);
        }
	else {
			$re = array('result'=> 419,'msg'=> "No Record Found",);
	}
}
else
{
   $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>